.. Copyright (c) 2018-2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


=========
C/C++/asm
=========

Types
=====

``executable``

   Produces an executable ``ELF`` file or binary file according to
   ``BIN_FORMAT``.

``shared-library``

   Produces a ``.so`` library. All symbols are hidden by default and a macro
   ``EXPORT`` is declared so that the user may selectively export the public
   facing interfaces.

``static-library``

   These are *not* the typical UNIX static libraries. These leverage the linker
   and ``objcopy`` to hide all symbols by default, behaving in the same way as
   the ``shared-library`` type and declaring the same ``EXPORT`` macro.

   The output for this type is a ``.o`` which doesn't allow the usage of the
   library search path feature of the linker and must be passed with full path
   to the linker of the executable that uses it.

``archive``

   This is the conventional ``.a`` UNIX static library as an archive.


Variables
=========

Generic
~~~~~~~

``DEBUG``

   There are three detectable levels for this variable in these rules starting
   at ``0``. These relate to optimization and debug flags that are automatically
   passed to the compiler and linker. Values above ``2`` behave the same as if
   ``2`` had been issued.


Specific
~~~~~~~~

``CROSS``, ``CC``, ``CXX``, ``AR``, ``LD``, ``OBJCOPY``, ``OBJDUMP``

   These variables specify the compilation toolchain.

``BIN_FORMAT``

   This variable has a single use case: to enable the generation of a binary
   file in addition to an ``ELF`` file for an ``executable`` item. To do so,
   this variable shall be set as ``true``. All other values are ignored.

``CFLAGS``

   These define a list of compilation flags for C and assembly source files.

``CXXFLAGS``

   Same as ``CFLAGS`` for C++ sources.

``LDFLAGS``

   Linker flags, passed to the linker *before* the object files.

``LIBS``

   Same as ``LDFLAGS``, but passed to the linker *after* the object files.

``SRC``

   This passes a list of all the ``.c``, ``.cpp`` and ``.S`` sources to be
   compiled for the particular ``dir.mk`` to the build system.

.. note::
   Variables that accept lists are deduplicated while preserving the order of
   appearance.


Debug keys
==========

``executable``, ``shared-library``, ``static-library``, ``archive``

   This filters for debug information about targets of their namesake type.


.. _ccppasm-templates:

Compilation templates
=====================

The build system supports some different templates. To use one of these
templates, the user shall call the function ``ccppasm-template`` with the
template name as the only parameter.

In doing so, some or all of the interface variables will be set to the template
default. The user is free to use them as they are, override or append anything
to them. This can only be done at the root of an item.

This list will not try to explain in detail what compilation options are
selected with each one. The user should read the files in question or inspect
the output to learn that.

``gcc-common``

   Typical |gcc| based toolchain with no assumption of cross compilation and
   with most useful warnings enabled as well as restrictions to the latest
   available C standard.

   This template defaults to thorough optimization and debugging levels
   according to ``DEBUG``.

``native-x86-64``

   Supports a typical UNIX environment with a |gcc| based toolchain on a
   ``x86-64`` machine. Includes ``gcc-common`` template.

``cross-cortex-common``

   Supports a cross build for ARM Cortex devices with a ``arm-none-eabi-gcc``
   based toolchain. Includes ``gcc-common`` template.

``cross-cortex-m4``

   Supports a cross build for ARM Cortex M4 microcontrollers. Includes
   ``cross-cortex-common`` template. This template enables and provides a
   default configuration for the ``openocd`` subtype.

``cross-cortex-m4f``

   Same as ``cross-cortex-m4`` for microcontrollers with an FPU. For these
   controllers, ``cross-cortex-m4`` may be used if the user has no interest in
   FPU support.


Advanced dependencies
=====================

The build system is made to track many dependencies automatically, but once
defined they're set in stone. Therefore there are circumstances in which the
build system does *not* enforce dependencies so as not create too many
dependencies that the user cannot undo.

Say we have a program ``prog`` that is linked against two different libraries,
one of which happens to be nested inside the program source folder, but not the
other one, like so::

   :
   |-- dir1/
   |   `-- ITEM=shared (compiles to libshared.so)
   |
   |-- dir2/
   |   |-- ITEM=prog (compiles to prog)
   |   |
   |   `-- dir3/
   |       `-- ITEM=archive (compiles to libarchive.a)
   |
   `-- ...

``prog`` can only be linked once ``libshared.so`` and ``libarchive.a`` are done.
Let's see what happens with each library.

``dir1`` and ``dir2`` are clearly too independent for the build system to assume
anything about their mutual relationship. Only looking at the linker flags could
the build system know or suspect the dependency. Short of overcomplicating, the
best option is to leave this up to the user to fix somehow.

The second case is more interesting though. Due to the build system automatic
rules, if we call ``make prog``, ``dir3`` is a dependency of ``dir2`` so
``libarchive.a`` will eventually be built. However, note that the automatic
dependency is (as it should be) in the directories. There is actually no
guarantee that ``prog`` will be linked after ``libarchive.a`` was built because
that dependency is not registered. Note that ``dir2`` depends on ``dir3`` which
means ``libarchive.a`` will be built before ``dir2``, but ``prog`` is still
independent and may well be scheduled in parallel with ``dir3`` and its
contents.

While we could infer this easily enough from the file system hierarchy, we'd be
creating dependencies where there may be none. Think of the Linux kernel and how
it may compile drivers as loadable modules or not. Preventing the Linux image
from linking until all modules were compiled would be pretty bad already, but
then the kernel image would be linked again every time a module changed. Since
this is a valid use case, we need to leave it to the user to fix again.

The user should then declare in the ``prog``'s makefile the dependencies
explicitly as shown. E.g.::

   _local_build_dir_ := $(call build-dir)

   $(_local_build_dir_)/prog:                        \
           $(_local_build_dir_)/archive/libarchive.a \
           $(call build-path)/tests/shared/libshared.so

This has a downside of making assumptions about the parent directory, namely the
path of the other library. An alternative would be to use relative paths using
``dir-fix`` to convert the whole path to its canonical format. E.g.::

   _local_build_dir_ := $(call build-dir)
   _root_build_dir_  := $(call dir-fix,$(_local_build_dir_)/..)

   $(_local_build_dir_)/prog:                        \
           $(_local_build_dir_)/archive/libarchive.a \
           $(_root_build_dir_)/shared/libshared.so

.. note::
   Note that all prerequisites of a linked target get passed to the linker. This
   has two consequences:

   * The first is that the user cannot make a dependency with a directory and he
     must always use the actual filenames with full paths of the libraries in
     the respective compile folder or the linker will complain that it can't
     find the file in question.

   * ``LIBS`` doesn't need to and in some cases shouldn't contain the same
     libraries again. This variable is still relevant to link against external
     libraries.
