# Copyright (c) 2018-2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Parse user variables
#


# = Parse user variables ===================================================== #

ifneq ($(strip $(_USR_ITEM_)),)

$(_ITEM_ID_)_DOC_CONF_ :=
$(_ITEM_ID_)_BUILDDIR_ := $(_DIR_BUILD_)

ifeq ($(strip $(SPHINX_BUILDERS)),)
$(_ITEM_ID_)_SPHINX_BUILDERS_ := dummy linkcheck html dirhtml singlehtml man latex latexpdf
else
$(_ITEM_ID_)_SPHINX_BUILDERS_ := $(SPHINX_BUILDERS)
endif

ifeq ($(strip $(DOC_DIR)),)
$(_ITEM_ID_)_DOC_DIR_ := $(_DIR_)
else
$(_ITEM_ID_)_DOC_DIR_ := $(call abspath,$(addprefix $(_DIR_)/,$(DOC_DIR)))
endif

endif

$(_ITEM_ID_)_DOC_CONF_ += $(call abspath,$(addprefix $(_DIR_)/,$(DOC_CONF)))
