.. Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


===================
Maker documentation
===================

.. toctree::
   :caption: Contents
   :maxdepth: 3

   introduction/index
   contributing/index
   design/index
   user-guide/index
   testing/index
