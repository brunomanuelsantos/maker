.. Copyright (c) 2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


=========
Framework
=========

Testing a generic build system has its difficulties. The framework designed for
this one is really simple, but not without some caveats. This should shed some
light on the decisions behind the current solution, instruct on how to use it
effectively and perhaps provide some guidance for future improvements.


Objective
=========

It's important to define what we're testing for that plays a role in defining
the framework. Here, we want to test the build system, not any external tool.

The things we want to test are then:

* The build commands.
* Rule configuration.
* The build concurrency.

The biggest hurdle here is that we don't want to pin down versions of external
tools. The build system should work well with a wide range of versions or even
different implementations of a given tool, even if their outputs differ
slightly. This means that the test output is dependent on the environment which
we do not want to control or test, making it difficult to compare the results
against an expected output.

Furthermore, commands and output alike may have paths in them that are specific
to the environment and user. Such references would be a sure way of breaking
tests on any machine other than the one they were developed for.

Accommodating the above in a test framework would be *very* difficult as it
would require highly customized regex matching of the output of each rule. Since
we want the framework to be simple, we solve this the only other way we can: by
not caring.

This leads us to the main aspect about this framework: this is *not* a
verification framework. This is a *delta analysis* tool, intended to find
differences in behaviour, rather than make absolute statements about it. In
fact, no expected output is provided at all in version control.


Procedure
=========

A test case is any (set of) targets and rules that may or may not be rigged to
fail. These are always declared within the ``tests`` directory with no
particular organization being required.

Besides the normal targets being generated, which can be very useful for
interactive testing, the Makefiles define extra convenience targets that
automate the delta analysis of two sets of output: ``update-tests-output`` and
``run-tests``.

These targets recursively invoke |make| in the ``tests`` directory to be able to
redirect the output, pass extra flags to |make| and to test the build system
from a different directory than the root Makefile, similar to what a user would
experience. However, regardless of how the tests are compiled, the output is
always placed in the same output folder by default and the compilation results
are (or should be) identical.

.. note::

   The test framework is not a substitute for running the tests interactively.
   The first detects sudden deviations from the previous behaviour, the latter
   allows the inspection of each step for correctness. E.g. to build all the
   tests from the project root, the user may issue a ``make build/tests -Bik``.

Now, to understand how it all works, let's analyse what happens when tests are
executed through ``run-tests`` (``update-test-output`` performs the same build
steps, but saving an expected output instead of comparing with it):

* Perform a dry run build (with ``--always-make`` and ``--dry-run``). This
  allows the comparison of the build commands.

* Build as much as possible (with ``--ignore-errors``, ``--always-make`` and
  ``--keep-going``). This allows the comparison of the build process itself.

* Perform a dry run ``clean`` (with ``--dry-run``). This checks the commands
  used for cleaning are the expected ones. There is no need to perform the
  actual cleaning (it's the same ``rm`` every time), so we leave that at the
  user's discretion.

Note that tests are executed with full concurrency, unless the user overrides it
calling |make| with ``--jobs`` of course. To maximize the concurrency
opportunities, test cases are not distinguishable from the perspective of the
test framework. I.e. they're all tested together in one step. However, the
developer still has fine grain control if he chooses to build the test targets
interactively.


Pass criteria
~~~~~~~~~~~~~

The pass criteria is trivial: the output must match the expected one. This is
ensured by calling ``diff`` on the standard output stream relative to the output
generated through the ``update-tests-output``.

.. tip::

   The user can override ``_DIFF`` from the environment or command line with a
   custom diff tool such as ``vimdiff``.


Caveats
~~~~~~~

Catch-22
--------

The pass criteria relies on the existence of some files capturing the expected
output. These are unique to each person's environment though so they need to be
generated locally. It should be obvious that generating the files just prior to
running the tests will result in 100% success rate apart from some concurrency
bugs. Clearly this isn't very useful when used like that.

The developer must understand where these tests are actually useful or there is
a real risk of the tests becoming a placebo if they pass or an annoyance if they
fail.


Non-deterministic execution order
---------------------------------

The tests are executed with default concurrency to maximize the test
effectiveness. Unfortunately the output becomes non-deterministic. We solve that
with ``sort`` at the expense of meaningful diffs.

If the test fails and the diff is not helpful, the developer may always resort
to executing the tests interactively, manually inspecting the output.

.. tip::

   The user can override ``_SORT`` from the environment or command line with a
   different program for sorting. E.g. using ``tee`` will disable sorting. In
   fact, by default, the sort is disabled when running with ``-j1``. Remember
   that you probably want to call the ``update-tests-output`` target with the
   same definitions though.


Non-deterministic rules
-----------------------

If a rule has some random or context dependent command parameter or output, we
can't test the correctness of this rule unless we override the rule itself with
a deterministic one. This would severely limit the scope of testing for that
rule, so such rules are probably better off *not* tested at all.

Fortunately, such rules should be fairly rare.
