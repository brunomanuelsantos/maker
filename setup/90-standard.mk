# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Standard targets that need initialization
#


# = Standard targets ========================================================= #

.PHONY: all
all:

.PHONY: clean
clean:

.PHONY: distclean
distclean:

ifneq ($(strip $(_BUILDPATH_)),$(call dir-fix,./))

# If building in a separate directory, we simply wipe that directory instead.
distclean:
	$(if $(wildcard $(_BUILDPATH_)),\
		$(call echo,"DISTCLEAN","$(_BUILDPATH_)","$(COLOUR_DEFAULT)"),)
	$(if $(wildcard $(_BUILDPATH_)),rm -rf -- $(_BUILDPATH_))

else

# If building 'in-source', we need to clean all files one by one.
distclean: clean
distclean: .distclean-indirect

.PHONY: .distclean-indirect
.distclean-indirect:

endif
