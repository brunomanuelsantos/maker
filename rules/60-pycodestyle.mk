# Copyright (c) 2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# pycodestyle (former PEP8) rules
#

ifneq ($(filter pycodestyle,$(_SUBTYPES_) $(_TYPE_)),)
$(call make-dbg,pycodestyle tree)

ifeq ($(strip $(_TYPE_)),)
$(call make-error,\
	"Pycodestyle rule requires an ITEM and TYPE to be defined as a subtype rule. "\
	"Consider using it as a type rule")
endif

# Parse interface variables.
include $(_RULE_DIR_)/pycodestyle/parse.mk

# Generate rules for the current item.
$(eval $(call pycodestyle-rule-gen,$(_ITEM_),$(_ITEM_ID_)))

endif
