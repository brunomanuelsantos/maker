# Copyright (c) 2018-2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Custom default configuration script for Sphinx.
#
# For detailed documentation on all the Sphinx configuration values see:
# http://www.sphinx-doc.org/en/master/config
#

import os
import sys
import datetime


# = General configuration ==================================================== #

# - Module paths & extensions ------------------------------------------------ #

sys.path.insert(0, os.path.abspath('modules'))

extensions = [
    # 'sphinx.ext.intersphinx',
    # 'sphinx.ext.napoleon',
    # 'hawkmoth'
]


# - Behaviour ---------------------------------------------------------------- #

nitpicky = True

source_suffix = {'.rst': 'restructuredtext'}


# = Documents ================================================================ #

# - Project stub configuration ----------------------------------------------- #

project = os.environ['_PROJECT_NAME_']
version = os.environ['_VERSION_']
release = '-'.join([version, os.environ['_CODE_NAME_']])

date      = datetime.datetime.now()
author    = os.environ['_AUTHOR_']
copyright = ', '.join([str(date.year), author])

language   = 'en'
master_doc = 'index'

highlight_language = 'text'


# - HTML options ------------------------------------------------------------- #

# If available, use the Furo theme. If not, don't break the build.
try:
    import furo
    html_theme = 'furo'

except ImportError:
    print('(Maker) Furo theme is not available. Using default unless overridden.',
          file=sys.stderr)


# - LaTeX options ------------------------------------------------------------ #

latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    'papersize': 'a4paper',

    # The font size ('10pt', '11pt' or '12pt').
    # 'pointsize': '10pt',

    # Additional stuff for the LaTeX preamble.
    # 'preamble': '',

    # Latex figure (float) alignment
    # 'figure_align': 'htbp',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#   author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc, 'myproject.tex', project + ' documentation', author, 'manual')
]


# - Man pages options -------------------------------------------------------- #

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, 'myproject', project + ' documentation', [author], 1)
]


# = Load user configuration ================================================== #
#
# Sphinx gets its configuration parameters from this script's namespace. So we
# only need to extend that namespace with whatever we want as if the code had
# been issued here.
#
# Do this last so that the user can override the variables.
#
# Finally, Sphinx configuration files will run in the original configuration
# file directory. However, for user provided configuration files, it makes sense
# that the directory is actually that of the configuration file. We save the
# user the legwork by fixing that here.

for conf in os.environ['_SPHX_CONF_'].split():
    print('(Maker) Processing additional configuration file:', conf)
    os.chdir(os.path.dirname(conf))
    exec(open(conf).read())
