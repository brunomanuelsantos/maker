# Copyright (c) 2018-2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Sphinx rules
#


ifneq ($(filter sphinx,$(_SUBTYPES_) $(_TYPE_)),)
$(call make-dbg,sphinx tree clean)

ifeq ($(strip $(_TYPE_)),)
$(call make-error,\
	"Sphinx rule requires an ITEM and TYPE to be defined as a subtype rule. "\
	"Consider using it as a type rule")
endif

# Parse interface variables.
include $(_RULE_DIR_)/sphinx/parse.mk

# Generate rules for the current item. This is the general case for rules that
# are not inter dependent.
_independent_builders_ := $(filter-out latex latexpdf,$($(_ITEM_ID_)_SPHINX_BUILDERS_))
$(foreach i,$(_independent_builders_),$(eval $(call sphinx-rule-gen,$(_ITEM_),$i,$i,$(_ITEM_ID_))))

# `latex` and `latexpdf` rules are inter dependent so we handle the special case
# here. Note that both rules are always created as long as the user specifies at
# least one of them.
ifneq ($(strip $(filter latex latexpdf,$($(_ITEM_ID_)_SPHINX_BUILDERS_))),)
$(eval $(call sphinx-rule-gen,$(_ITEM_),latex,latex,$(_ITEM_ID_)))
$(eval $(call sphinx-rule-gen,$(_ITEM_),latex,latexpdf,$(_ITEM_ID_)))
$(_DIR_BUILD_)-latexpdf: $(_DIR_BUILD_)-latex
endif

# Create a target alias for the HTML build with the item name.
ifneq ($(strip $(filter html,$($(_ITEM_ID_)_SPHINX_BUILDERS_))),)
.PHONY: $(_ITEM_)
$(_ITEM_): $(_ITEM_)-html
endif

_clean_dirs_ := doctrees $(filter-out dummy latexpdf,$($(_ITEM_ID_)_SPHINX_BUILDERS_))
$(eval _clean_ := $(addprefix $(_DIR_BUILD_)/,$(_clean_dirs_)))
$(foreach i,$(_clean_),$(eval $(call clean-gen,$i,$(COLOUR_SPHX),)))

endif
