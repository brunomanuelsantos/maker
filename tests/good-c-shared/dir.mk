# Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later

TYPE := shared-library
ITEM := shared

$(call ccppasm-template,native-x86-64)

SRC := lib.c
