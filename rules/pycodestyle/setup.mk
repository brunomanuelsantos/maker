# Copyright (c) 2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0


# = Constants ================================================================ #

# - Colour codes ------------------------------------------------------------- #

COLOUR_PYCODESTYLE ?= $(COLOUR_BOLD)$(COLOUR_YELLOW)


# = Rules ==================================================================== #

# - Pycodestyle rule generator ----------------------------------------------- #
#
# This takes the following parameters:
# 1. Item name.
# 2. Item ID.
define pycodestyle-rule-gen
.PHONY: $1-pycodestyle
$1-pycodestyle: _item_id_ := $2
$1-pycodestyle:
	$(call echo,"PYCODESTYLE","$($(_item_id_)_DIR_)","$(COLOUR_PYCODESTYLE)")
	pycodestyle --config="$$($$(_item_id_)_STYLE_CONF_)" \
		$$($$(_item_id_)_STYLE_SRC_)

.PHONY: $1-pycodestyle-first
$1-pycodestyle-first: _item_id_ := $2
$1-pycodestyle-first:
	$(call echo,"PYCODESTYLE","$($(_item_id_)_DIR_)","$(COLOUR_PYCODESTYLE)")
	pycodestyle --config="$$($$(_item_id_)_STYLE_CONF_)" --first \
		$$($$(_item_id_)_STYLE_SRC_)

.PHONY: $1-pycodestyle-stat
$1-pycodestyle-stat: _item_id_ := $2
$1-pycodestyle-stat:
	$(call echo,"PYCODESTYLE","$($(_item_id_)_DIR_)","$(COLOUR_PYCODESTYLE)")
	pycodestyle --config="$$($$(_item_id_)_STYLE_CONF_)" -qq --statistics \
		$$($$(_item_id_)_STYLE_SRC_)
endef


# = Clean up ================================================================= #

_CLEANUP_ += STYLE_SRC STYLE_CONF
