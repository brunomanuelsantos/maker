/*
 * Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* Key memory addresses as defined in the linker script */
extern unsigned long _istack_ptr;
extern unsigned long _data_lma;
extern unsigned long _data_start;
extern unsigned long _data_end;
extern unsigned long _bss_start;
extern unsigned long _bss_end;

/* Forward declaration of main. */
int main(void);

/* Entry point. */
void _start(void)
{
	unsigned long *lma;
	unsigned long *vma;

	/* Copy data from load to virtual addresses (Flash to RAM). */
	lma = &_data_lma;
	for (vma = &_data_start; vma < &_data_end; vma++, lma++) {
		*vma = *lma;
	}

	/* Default undefined variables to zero. */
	for (vma = &_bss_start; vma < &_bss_end; vma++) {
		*vma = 0u;
	}

	/* Finished memory initialization; call main(). */
	main();
}

/* Simplest functional interrupt vector. */
void (*const isr_vector[])(void) __attribute__ ((section(".nvic_table"))) = {
	(void *) &_istack_ptr,
	_start
};
