.. Copyright (c) 2018-2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


.. |doxygen|     replace:: *Doxygen*
.. |exception|   replace:: ``maker-exception-1.0``
.. |gcc|         replace:: *GCC*
.. |html|        replace:: *HTML*
.. |latex|       replace:: *LaTeX*
.. |license|     replace:: ``GPL-3.0-or-later``
.. |make|        replace:: *Make*
.. |openocd|     replace:: *OpenOCD*
.. |pycodestyle| replace:: *Pycodestyle*
.. |source|      replace:: https:\/\/gitlab.com\/brunomanuelsantos\/maker
.. |spdx|        replace:: *SPDX*
.. |sphinx|      replace:: *Sphinx*
