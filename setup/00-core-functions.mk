# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Auxiliary functions and macros
#


# = Directory resolving ====================================================== #
#
# Tread carefully, the build system relies on very specific behaviour out of
# these functions. A space in the wrong place and all hell breaks loose.
#
# Pay special attention to situations where these would return nothing or './'
# or to cases where it could return './foo' or 'foo'. Changing the behaviour may
# totally break the build system or seriously impact the user.

# - Fix path ----------------------------------------------------------------- #
#
# This resolves the given (list of) path(s) relative to the directory in which
# make was called from *or* the file system root if the path is above it. While
# the build system could potentially do away with this (e.g. double slashes not
# a problem), this results in paths which are much nicer to read and easier to
# guess for the user since these play a role in defining some targets.

dir-fix = $(strip \
	$(foreach i,$1,$(shell realpath -m -z -s --relative-base=./ $i)))


# - Resolve current path ----------------------------------------------------- #
#
# This function resolves the directory of the makefile in which it's called
# provided that it's not called *after* an include directive. This is because it
# uses the list of included makefiles to extract the directory.
#
# Technically, this returns the path of the last included makefile.

pwd = $(call dir-fix,$(dir $(lastword $(MAKEFILE_LIST))))


# - Directory ancestry check ------------------------------------------------- #

# Find parent directories of $1 in $2.
parents = $(foreach i,$2,$(if $(filter $i,.),$i,$(if $(filter $i/%,$1),$i,)))


# = Variable foo ============================================================= #

# - Special variable tests --------------------------------------------------- #

# Test if variable is defined at all.
ifdef = $(if $(filter-out undefined,$(origin $(strip $1))),$2,$3)


# - Filter variables --------------------------------------------------------- #

# Deduplicate list of variables retaining order of appearance.
define dedup =
$(strip
	$(eval _ret_ :=)
	$(foreach i,$1,$(if $(filter $i,$(_ret_)),,$(eval _ret_ += $i)))
	$(_ret_))
endef


# = Stack like operations ==================================================== #
#
# Intended usage implies self referencing and therefore using ':=' assignments.
# Here, the stack is conceptualized as having it's most recent element in the
# first position.
#
# To actually access the variables, [first,last]word and wordlist commands may
# be used bearing in mind the top variable is always the first on the list.

# Push variable(s) $1 onto stack $2.
#   stack := $(call push,$(new),$(stack))
push = $1 $2

# Pop $1 number of elements from stack $2.
#   stack := $(call pop,1,$(stack))
pop = $(wordlist $(shell expr $1 + 1),$(words $2),$2)


# = Rule generators ========================================================== #
#
# If it's worth repeating, it's worth automating. These are rules generators for
# common patterns across the build system.

# - Clean targets ------------------------------------------------------------ #
#
# This declares a 'clean' rule for a given file / folder; add dependency on the
# new rule to the main clean rule and to the respective directory clean rule.
#
# If the current directory is '.', then there's no point in declaring a separate
# rule as it would be equivalent to 'clean'. We still declare it if building out
# of source though. This is a bit obscure, but it allows every tab expanded
# target to be valid: 'clean-./dir' wouldn't be valid because we declare it as
# 'clean-dir' and 'clean-build-dir' would be expanded even if it didn't exist as
# long as there were 'clean-build-dir/dir1' and 'clean-build-dir/dir2'.
#
# This takes 3 parameters:
#
# 1. Name of the target to create (or list of names for batch mode).
# 2. Colour escape sequence for the output.
# 3. Optional. If not empty, disables the deletion. This is useful to declare
#    clean rules that serve as dependency 'glue' between others, but which
#    shouldn't do anything themselves.

clean-gen-batch = $(foreach i,$1,$(eval $(call clean-gen,$(i),$2,$3)))

define clean-gen
.PHONY: clean-$1
clean-$1:
	$(if $3,,$(if $(wildcard $1),
	$(call echo,"CLEAN","$1","$2")
	rm -rf -- $1,))

clean: clean-$1

$(if $(filter .,$(_DIR_BUILD_)),,clean-$(_DIR_BUILD_): clean-$1)

$(call make-dbg,clean,"Clean:     $1")
endef


# - Distclean targets -------------------------------------------------------- #
#
# Similar to the clean rules generator, except for two points:
#
# 1. A directory distclean depends on the corresponding directory clean. This is
#    currently implemented in such a way that the dependency is declared
#    multiple times, but it avoids handling even more special cases.
# 2. We don't want to assign dependencies directly to distclean as documented in
#    its original declaration. We use an indirection instead so that we can
#    simplify distclean in out of source builds.

distc-gen-batch = $(foreach i,$1,$(eval $(call distc-gen,$(i),$2,$3)))

define distc-gen
.PHONY: distclean-$1
distclean-$1:
	$(if $3,,$(if $(wildcard $1),
	$(call echo,"DISTCLEAN","$1","$2")
	rm -rf -- $1,))

.distclean-indirect: distclean-$1

$(if $(filter .,$(_DIR_BUILD_)),,distclean-$(_DIR_BUILD_): clean-$(_DIR_BUILD_) distclean-$1)

$(call make-dbg,clean,"Distclean: $1")
endef
