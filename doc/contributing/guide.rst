.. Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


==================
Submitting patches
==================

This project is hosted at |source|.

The preferred way to submit patches or pull requests is through the project's
mailing list. Submitting them through the project's GitLab page is also
acceptable.


======
Issues
======

Please use the GitLab issue tracker.


=========
Copyright
=========

Assignment
==========

All contributors are encouraged to retain their individual copyrights. All files
should have at least one copyright notice in the form of::

   Copyright (c) 2000 John Doe <john.doe@email.com>
   Copyright (c) 2000-2001 Jane Doe <jane.doe@email.com>


Licensing
=========

This project is licensed under the terms of the |license| with |exception|.
Licensing follows the `SPDX standard <https://spdx.org>`_ and each file may
specify special exceptions or possibly different yet compatible licenses.

The arbitration on the effective license shall be, from the highest to the
lowest priority:

#. Source file licensing, although this shall be avoided in favour of |spdx|
   license codes. This should only apply to files that are imported 'as is' like
   license files themselves.

#. Appropriately linked |spdx| licenses and exceptions as defined in the
   ``LICENSE`` folder.

#. In the absence of any licensing information, a single project level license
   shall apply without exceptions: |license|.

The |spdx| license code shall be applied with the usual guidelines except where
the choice of commenting style is in conflict with any other coding style
guideline.
