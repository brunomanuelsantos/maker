.. Copyright (c) 2018-2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


===========
Core design
===========

A simple overview of the build system can be seen in its file organization as
illustrated below::

   .
   |-- Makefile
   |
   |-- setup/
   |   |-- 00-core-functions.mk
   |   |-- 00-public-functions.mk
   |   |-- ...
   |   `-- 99-rule-setup.mk
   |
   `-- rules/
       |-- 00-preamble.mk
       |-- 10-parse.mk
       |-- ...
       |  
       |-- rule/
       |   |-- setup.mk
       |   `-- ...
       |-- xx-rule.mk
       |  
       |-- ...
       |-- 95-cleanup.mk
       `-- 99-subdirs.mk

As shown there are two fundamental subfolders to the build system: the ``setup``
and the ``rule`` folders. The first holds files that are parsed once, early in
the main makefile, the second holds rules that will be included for each
subdirectory of the source tree.


The Makefile
============

Despite the description, this makefile *is* recursive, except it has a single
recursive call of itself. This is not for the sake of the build, but rather to
allow us to control |make|'s options. The same could be achieved with no
recursion at all at the expense of some aliases, extra typing, environment
variables or combinations of these.

This is a very simple root makefile that is expected to be symbolic linked to
the root of the project being built and its ultimate goal is to bootstrap the
recursive rules. Note that recursiveness here refers only to the recursive
traversal of the source tree, not the |make| invocation.


One off setup
=============

The setup folder hosts makefiles which are to be included only once, early in
the makefile parsing. This job is the last to be performed directly by the main
makefile and culminates at the very end with the bootstrapping of the recursive
rules.

Any makefile within this directory is included in alphabetical order without any
other discrimination. No subdirectories are supported.

To support one off initializations necessary for some rules, this process also
includes all rule specific setup makefiles it finds nested inside the rules
directory. These need to be called ``setup.mk`` and be exactly 1 level deep
inside of the rules' folder.


Source tree traversal rules
===========================

Among the rules, there are a few particularly important as the glue logic that
enables all the others at relatively low development and maintenance cost.

These are otherwise indistinguishable from the build rules and are actually
bootstrapped by the main makefile together with them. Their purpose is to keep
descending into every subdirectory of the source tree automatically and setting
up some variables on behalf of the build rules (the first rules being processed)
and the user (the last ones before including a new ``dir.mk`` file.

Similarly to the setup makefiles, these are included in alphabetical order.

These are:

``00-preamble.mk``

   This is always the first thing to be done once entering a new directory. It
   will resolve the name of this directory and otherwise clear the way for
   bigger and greater things.

``10-parse.mk``

   This makefile takes the variables defined in the users' ``dir.mk`` file of a
   given directory and decides what needs to be done in that directory. This is
   achieved by keeping a stack of directories that define a *type* and an *item*
   to be built so it can push and pop these from the stack as it traverses the
   source tree depth first.

   This allows the context to be regained once leaving a nested build back onto
   the parent folder which may be building something entirely different.

``95-cleanup.mk``

   Since the variables the user has available to communicate its intention with
   the build system are the same for any arbitrarily large number of
   subdirectories, it follows that we need to clear the interface variables
   before reading a new set from a new directory and a new round of rules
   inclusion. Otherwise the user would be forced to empty all those variables
   and keep track of them himself. This is accomplished here.

   In case the distinction between an empty variable and an undefined one is
   necessary, this step completely undefines them entirely.

   All of these clean up steps must be taken unconditionally and rely on rules
   to append their own interface variables to the relevant internal variable.

``99-subdirs.mk``

   This makefile will include a list of makefiles composed of all the
   subdirectories' ``dir.mk`` files interspersed with all the rules::

      dir_1/dir.mk rule_1.mk ... rule_n.mk dir_2/dir.mk rule_1.mk ... rule_n.mk.

   Since ``rule_n.mk`` is always ``99-subdirs.mk``, this include keeps
   descending through all the directories depth first. The traversal is always
   done in the order in which the user specifies the subdirectories.

Subdirectories are supported down to any number of levels, but not as part of
the core build system. This support exists for the benefit of writing rules that
may be arbitrarily complex and therefore split in multiple files or include
extra files that are not makefiles.


Automatic targets
~~~~~~~~~~~~~~~~~

This build system not only tracks the whole dependency tree, it does so creating
plenty of targets along the way as both a feature and a result of such decision.

The base of the build system provides targets or target generators (e.g. clean
targets) with the ultimate goal that ever single file and directory has a target
for building *and* cleaning, with full dependency resolution.

The very nature of the build system putting together a full dependency tree
means a lot of tab expansion goodness for those who use it. And even without tab
expansion, the user can then build or clean any file or subfolder separately.


Rule flavours
=============

The build system supports two flavours of rules: type and subtype rules. Of note
is that there are hardly any difference between them, especially from an
implementation point of view. It's the build system core that gives each the
different semantics. In fact, a rule may double as both at its discretion.

So why two types? This covers cases where a rule can be used to extend a full
fledged type rule. This happens for instance if a helper debug or packaging
target can be useful regardless of the chosen compilation rule. Essentially, any
rule that doesn't operate on the sources, but on the output, or that merely
automates some workflow process can and probably should be made a subtype rule.


Type rules
~~~~~~~~~~

These are *item* centric and therefore recursive by nature. As long as there are
subdirectories, these rules will keep applying to newly found makefiles.

There can only be one type rule per *item*.


Subtype rules
~~~~~~~~~~~~~

These rules are only active in the directory they're defined as far as the build
system is concerned. I.e. they won't be invoked again for the respective
subdirectories if there are any.

These rules may be selected by default or at the user's request to extend a type
rule, but on the latter case, whether some combination make sense or not is not
the build system's concern.

There can be multiple subtype rules active for any given directory. However the
user should check for possible interface variable conflicts. E.g. if two
different rules expect two different things in the same interface variable, it's
unlikely they will work without patching.

They may also define targets with the same name, in which case |make| will
loudly complain about rule redefinition. There's little to do in both cases
other than patching the names to be different.

From the core perspective, these rules are the simplest and most versatile ones
as there are no assumptions on how they behave. They don't even need an *item*
to be defined as far as the build system is concerned. Such versatility should
not be considered carte blanche to reinvent the build system itself.


.. _why-not-vpath:

Why not VPATH
=============

Using ``VPATH`` allows simple rules to be written while supporting out of source
builds. This simplicity would come from the fact that rules wouldn't need to
append the build directory in their definitions.

It would also arguably improve the target names in out of source builds, which
would be the same as if it were an in source compilation. This can be
particularly good in shells that have no rule tab expansion for |make|, but that
still have file name expansion.

So, why not ``VPATH``?

#. Having a single scope (non-recursiveness) means we have a single (e.g.) ``.o:
   .c`` rule. Since we want to support multiple items with possibly different
   compilers and flags, it follows that we need to be able to configure the same
   rule differently for each case.

   There are two ways to achieve this and we can solve the issue with either of
   them alone or together, *regardless* of using ``VPATH``: prepending path
   names when declaring recipes and overriding rule variables like
   ``path/to/output.file: var := override``

   This voids the first stated advantage.

#. Explicitness of which file or folder we want to reference is lost once we
   have more than one build path (e.g. a production and debug build). The user
   could be easily tricked into thinking he had compiled what he wanted when in
   fact he had not. In contrast, the current solution forces an error if the
   user tries to build a file that doesn't correspond to the selected
   configuration.

   This may not void, but at least questions the second stated advantage.

#. If calling ``make`` from the source directory, redirecting output to a
   different folder is far from trivial if one doesn't want usability penalties.
   There are `ways
   <http://make.mad-scientist.net/papers/multi-architecture-builds/>`_ of doing
   so, but then the example is still a simple one.

   Most of these usability quirks would probably take just as much ugliness to
   fix as it could be saved, other points not withstanding.
