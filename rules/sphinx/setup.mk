# Copyright (c) 2018-2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Setup some one off variables
#


# = Constants ================================================================ #

# - Verbosity ---------------------------------------------------------------- #
#
# Sadly Sphinx outputs everything to stdout, be it success messages, warnings or
# fatal errors (unless python itself throws with no catch I guess). This means
# that we need to take an extreme decision: hide or show everything by default.
#
# We choose the 1st since there can be a *lot* of output, especially if LaTeX is
# involved. The user will need to enable the verbosity to verify that this gets
# compiled correctly though.

ifneq ($(strip $(_VERBOSITY_)),0)
_SPHX_QUIET_ :=
else
_SPHX_QUIET_ := &> /dev/null
endif


# - Colour codes ------------------------------------------------------------- #

COLOUR_SPHX ?= $(COLOUR_BOLD)$(COLOUR_YELLOW)


# = Rules ==================================================================== #

# Main conf.py directory.
_SPHX_MAIN_CONF_ := $(_RULE_DIR_)/sphinx


# - Sphinx rule generator ---------------------------------------------------- #
#
# All Sphinx rules are all very similar, so we can generate any desired subset
# easily. These need to be instantiated on the main rule makefile since these
# are not and cannot be generic recipes.
#
# This takes the following parameters:
# 1. Item name.
# 2. Sphinx builder name. This doubles as the name of the sub-directory in which
#    the output is placed. This is due to the usage of make mode (-M).
# 3. Sphinx make mode rule. This is often the same as the builder name, but not
#    always. E.g. latexpdf operates on the output of latex and therefore creates
#    no extra output directory.
# 4. Item ID.
define sphinx-rule-gen
.PHONY: $1-$3
$1-$3: _item_id_ := $4
$1-$3:
	$(call echo,"SPHX","$$($$(_item_id_)_BUILDDIR_)/$2 ($3)","$(COLOUR_SPHX)")
	_SPHX_CONF_="$$($$(_item_id_)_DOC_CONF_)" sphinx-build -M $3           \
		    "$$($$(_item_id_)_DOC_DIR_)" "$$($$(_item_id_)_BUILDDIR_)" \
		    -j auto -c "$(_SPHX_MAIN_CONF_)" $(_SPHX_QUIET_)

$(_DIR_BUILD_): $1-$3
$(call make-dbg,sphinx tree,"New dependency: $(_DIR_BUILD_): $1-$3")
endef


# = Clean up ================================================================= #

_CLEANUP_ += SPHINX_BUILDERS DOC_DIR DOC_CONF
