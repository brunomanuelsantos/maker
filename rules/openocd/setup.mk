# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Setup some one off variables
#


# = Clean up ================================================================= #

_CLEANUP_ += CROSS GDB OPENOCD
_CLEANUP_ += ELF BIN
_CLEANUP_ += LINK TARGET ADDRESS
