# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Build system variables
#

# = Colour codes ============================================================= #
#
# These have no functionality impact (unless user writes something stupid to
# them perhaps), so we won't bother with making sure these don't change. In
# fact, we'll treat these as if they're global, one off variables.

# - Shell escape codes ------------------------------------------------------- #

ifneq ($(strip $(COLOUR)),no)
COLOUR_BLACK     ?= \e[30m
COLOUR_RED       ?= \e[31m
COLOUR_GREEN     ?= \e[32m
COLOUR_YELLOW    ?= \e[33m
COLOUR_BLUE      ?= \e[34m
COLOUR_PURPLE    ?= \e[35m
COLOUR_CYAN      ?= \e[36m
COLOUR_WHITE     ?= \e[37m
COLOUR_ON_BLACK  ?= \e[40m
COLOUR_ON_RED    ?= \e[41m
COLOUR_ON_GREEN  ?= \e[42m
COLOUR_ON_YELLOW ?= \e[43m
COLOUR_ON_BLUE   ?= \e[44m
COLOUR_ON_PURPLE ?= \e[45m
COLOUR_ON_CYAN   ?= \e[46m
COLOUR_ON_WHITE  ?= \e[47m
COLOUR_BOLD      ?= \e[1m
COLOUR_DIM       ?= \e[2m
COLOUR_EMPH      ?= \e[3m
COLOUR_UNDERLINE ?= \e[4m
COLOUR_HIGHLIGHT ?= \e[7m
COLOUR_RESET     ?= \e[m
else
COLOUR_BLACK     =
COLOUR_RED       =
COLOUR_GREEN     =
COLOUR_YELLOW    =
COLOUR_BLUE      =
COLOUR_PURPLE    =
COLOUR_CYAN      =
COLOUR_WHITE     =
COLOUR_ON_BLACK  =
COLOUR_ON_RED    =
COLOUR_ON_GREEN  =
COLOUR_ON_YELLOW =
COLOUR_ON_BLUE   =
COLOUR_ON_PURPLE =
COLOUR_ON_CYAN   =
COLOUR_ON_WHITE  =
COLOUR_RESET     =
COLOUR_BOLD      =
COLOUR_DIM       =
COLOUR_EMPH      =
COLOUR_UNDERLINE =
COLOUR_HIGHLIGHT =
endif


# - Global colour codes ------------------------------------------------------ #

COLOUR_DEFAULT   ?= $(COLOUR_BOLD)
COLOUR_MAKEDBG   ?= $(COLOUR_BOLD)$(COLOUR_BLUE)
COLOUR_MAKEERROR ?= $(COLOUR_BOLD)$(COLOUR_RED)


# = Build system configuration =============================================== #
#
# Some variables of the build system require initialization before their first
# use. These should be the exception rather than the norm.
#
# It's also an opportunity to enforce that any further change to these variables
# by the user will not be acknowledged. To do so, we'll keep an internal
# snapshot of these.


# - Makefile debug ----------------------------------------------------------- #

MAKEDBG ?=
_MAKEDBG_ := $(MAKEDBG)


# - Verbosity level ---------------------------------------------------------- #

VERBOSITY ?= 0
_VERBOSITY_ := $(VERBOSITY)


# - Debug -------------------------------------------------------------------- #

DEBUG ?= 0
_DEBUG_ := $(DEBUG)


# - Build path --------------------------------------------------------------- #

ifeq ($(strip $(BUILDPATH)),)
$(call make-error,"Build path not specified")
endif

# Sanitize the path.
_BUILDPATH_ := $(call dir-fix,$(BUILDPATH))

# Append a debug suffix for out of source builds.
ifneq ($(strip $(_BUILDPATH_)),$(call dir-fix,./))
ifneq ($(strip $(_DEBUG_)),0)
_BUILDPATH_ := $(_BUILDPATH_)-dbg$(_DEBUG_)
endif
endif
