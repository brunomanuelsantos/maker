# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later


PROJECT_NAME = Maker
CODE_NAME    = Spagheti Monster
VERSION      = $(shell git describe --dirty --tags --always)
AUTHOR       = Bruno Santos

BUILDPATH ?= build

all: doc-html
