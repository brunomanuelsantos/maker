# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Typical GCC toolchain configuration
#


# = Toolchain ================================================================ #

CROSS   :=
CC      := gcc
CXX     := g++
AR      := ar
LD      := ld
OBJCOPY := objcopy
OBJDUMP := objdump
GDB     := gdb

BIN_FORMAT :=


# = Compilation flags ======================================================== #

# - Seed flags --------------------------------------------------------------- #

CFLAGS   := -std=c11
CXXFLAGS := -std=c++14
LDFLAGS  := -Wl,-O1,--as-needed
LIBS     :=


# - Special target flags ----------------------------------------------------- #
#
# Extra compile flags: hide all symbols by default and define an EXPORT macro
# which will make a symbol visible.
#
# Additionally, shared libraries shall be compiled with position independent
# code.

_STATIC_LIB_CFLAGS_   := -fvisibility=hidden
_STATIC_LIB_CFLAGS_   += -DEXPORT='__attribute__((visibility("default")))'

_STATIC_LIB_CXXFLAGS_ := $(_STATIC_LIB_CFLAGS_)

_SHARED_LIB_CFLAGS_   := -fPIC $(_STATIC_LIB_CFLAGS_)
_SHARED_LIB_CXXFLAGS_ := -fPIC $(_STATIC_LIB_CXXFLAGS_)


# - Common error flags ------------------------------------------------------- #

_err_flags_  = -Wall -Wextra
_err_flags_ += -Wunused
_err_flags_ += -Wformat=2
_err_flags_ += -Wrestrict
_err_flags_ += -Wfloat-equal
_err_flags_ += -Wredundant-decls
_err_flags_ += -Wshadow
_err_flags_ += -Wno-packed-bitfield-compat

CFLAGS   += $(_err_flags_) -Wstrict-prototypes
CXXFLAGS += $(_err_flags_)


# - Optimization & debug flags ----------------------------------------------- #

_opt_flags_ :=

ifeq ($(strip $(_DEBUG_)),0)
CFLAGS   += -O2 $(_opt_flags_) -DNDEBUG
CXXFLAGS += -O2 $(_opt_flags_) -DNDEBUG
LDFLAGS  += -O2 $(_opt_flags_)
endif
ifeq ($(strip $(_DEBUG_)),1)
CFLAGS   += -O2 $(_opt_flags_) -ggdb3
CXXFLAGS += -O2 $(_opt_flags_) -ggdb3
LDFLAGS  += -O2 $(_opt_flags_) -ggdb3
endif
ifeq ($(strip $(filter 0 1,$(_DEBUG_))),)
_opt_flags_ += -fstrict-aliasing
CFLAGS      += -Og $(_opt_flags_) -ggdb3
CXXFLAGS    += -Og $(_opt_flags_) -ggdb3
LDFLAGS     += -Og $(_opt_flags_) -ggdb3
endif
