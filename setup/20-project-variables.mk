# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Project variables
#

# Ensure these are not empty.
PROJECT_NAME ?= unknown
CODE_NAME    ?= unknown
VERSION      ?= unknown
AUTHOR       ?= unknown

# Keep copies the user is not allowed (gentleman's agreement) to change.
_PROJECT_NAME_ := $(PROJECT_NAME)
_CODE_NAME_    := $(call code-name,$(CODE_NAME))
_VERSION_      := $(VERSION)
_AUTHOR_       := $(AUTHOR)

# Export variables for usage in other tools. This is particularly useful for
# documentation targets.
export _PROJECT_NAME_
export _CODE_NAME_
export _VERSION_
export _AUTHOR_
