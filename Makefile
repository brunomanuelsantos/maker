# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Main makefile
#


# = Launch make automagically as intended ==================================== #
#
# This allows all targets to be ignored and passed instead to a nested
# invocation of make whose invocation we can control.

ifeq ($(strip $(_LAST_INVOCATION_)),)


# - Parallel make ------------------------------------------------------------ #
#
# We want to go as fast as possible by default, so we add a '-j' option to the
# recursive call.
#
# However, the '-j' is a special flag in that it can't be specified again for a
# recursive call if it was already specified for the top call. Doing so results
# in a warning and parallel compilation to be degraded to '-j1'. This is
# important because the user or ports-like package managers must be able to
# override it.
#
# Now the tricky part: MAKEFLAGS doesn't contain the '-j' option unless it's
# evaluated at rule expansion time. Therefore we need to test it in the sub-make
# target itself. Go figure...

_jobs_ = -j$(shell nproc)


# - Verbosity ---------------------------------------------------------------- #
#
# By default, we want make to be fast and spare us the gory details. We'll
# enforce that by making silent builds by default.
#
# We want the user to be able to override these somehow. There's no getting
# around the silencing flags, but there should be little point in compiling
# *and* looking at the commands at the same time. I.e. the user can get by with
# 'make -n'.

_makeflags_ = -s --no-print-directory


# - Some more options -------------------------------------------------------- #
#
# * Mark the following make invocation as the last one.
# * Disable all internal rules and variables.
# * Check symbolic links' times as well, not just the target.

_makeflags_ += _LAST_INVOCATION_=yes
_makeflags_ += -rR
_makeflags_ += -L


# - Bypass user input, do something else first ------------------------------- #
#
# We need to keep track of what the user asked for so we can pass it on to the
# final and effective make invocation.
#
# If there's no target, 'all' will be the implicit goal.

_goals_ := $(filter-out all .sub-make,$(MAKECMDGOALS))

.PHONY: all $(_goals_)
all $(_goals_): .sub-make


# - Call make, now in our terms ---------------------------------------------- #

.PHONY: .sub-make
.sub-make:
	@$(MAKE) $(_makeflags_) $(if $(filter -j%,$(MAKEFLAGS)),,$(_jobs_)) $(_goals_)

endif


# = Finally... =============================================================== #
#
# This is where we start parsing all the makefiles.

ifeq ($(strip $(_LAST_INVOCATION_)),yes)


# - Second expansion --------------------------------------------------------- #
#
# This enables us to declare some targets when not all the dependencies have
# been worked out yet. E.g.:
#
#   program: $$(program_obj)
#
# Where 'program_obj' is incomplete because make hasn't descended into relevant
# subdirectories.

.SECONDEXPANSION:


# - Find the root of the build system ---------------------------------------- #
#
# Get the path to the build system root. Follow symlink if necessary.

_MAKEDIR_ := $(dir $(shell realpath -m -z --relative-base=./ $(lastword $(MAKEFILE_LIST))))


# - Project configuration ---------------------------------------------------- #
#
# Include project configuration variables. Must be included before the setup as
# we may want to to do some tidying up after the user.

-include project.mk


# - Automatic setup ---------------------------------------------------------- #
#
# Include one time setup files. This will culminate in the bootstrapping of all
# the rules.

include $(sort $(wildcard $(_MAKEDIR_)/setup/*.mk))

endif
