# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Template selection
#


# = Determine target and load defaults ======================================= #

ifeq ($(strip $(TYPE)),)
$(call make-error,"Templates may only be selected at the item's root")
endif

ifeq ($(strip $(_TEMPLATE_)),)
$(call make-error,"Template not specified")
endif

_template_ := $(call pwd)/templates/$(_TEMPLATE_).mk
ifeq ($(wildcard $(_template_)),)
$(call make-error,"Template '$(_TEMPLATE_)' not found")
endif

-include $(_template_)
