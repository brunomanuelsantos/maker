# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Toolchain template for x86-64 native compilation
#


# Load the default toolchain configuration.
include $(_RULE_DIR_)/c-cpp-asm/templates/gcc-common.mk

CROSS    :=

CFLAGS   += -march=x86-64 -mtune=generic
CXXFLAGS +=
LDFLAGS  +=
LIBS     +=
