# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later

# Include the abbreviations file globally.
rst_epilog = open('abbreviations.inc', 'r').read()
