# Copyright (c) 2018-2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later


TYPE = sphinx
ITEM = doc

SPHINX_BUILDERS = html latex

DOC_CONF = conf.py
