# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Toolchain template for ARM Cortex M4F cross compilation
#


# Include common definitions for Cortex targets.
include $(_RULE_DIR_)/c-cpp-asm/templates/cross-cortex-common.mk

CFLAGS   += -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16
CXXFLAGS +=
LDFLAGS  +=
LIBS     +=

# Tentatively set OpenOCD subtype interface variables on behalf of the user.
TARGET := target/stm32f4x.cfg
