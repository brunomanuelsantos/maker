# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Doxygen rules
#


ifneq ($(filter doxygen,$(_SUBTYPES_) $(_TYPE_)),)
$(call make-dbg,doxygen tree clean)

ifeq ($(strip $(_TYPE_)),)
$(call make-error,\
	"Doxygen rule requires an ITEM and TYPE to be defined as a subtype rule. "\
	"Consider using it as a type rule")
endif

# Parse interface variables.
include $(_RULE_DIR_)/doxygen/parse.mk

# Collection of files / directories to be cleaned.
_clean_ :=


# = Build rules ============================================================== #

# - All targets -------------------------------------------------------------- #
#
# Some Doxygen builds require 2 (or more?) passes. E.g. generate LaTeX sources
# and compile them. All of them need the same 1st pass though.
#
# This target is not meant to be called directly, so we use a unique but hidden
# handle for it.

.PHONY: .$(_ITEM_ID_)
.$(_ITEM_ID_): export _dox_dir_   := $(_DIR_BUILD_)
.$(_ITEM_ID_): export _user_doxy_ := $($(_DIR_)_CONF_)
.$(_ITEM_ID_): $(_RULE_DIR_)/doxygen/Doxyfile
	mkdir -p $(_dox_dir_)
	$(call echo,"DOX","$(_dox_dir_)","$(COLOUR_DOX)")
	doxygen $<


# - HTML --------------------------------------------------------------------- #
#
# This is the default documentation build, so we use the raw item name as a way
# of building it.

ifeq ($($(_DIR_)_DOX_HTML_),YES)

# Convenience target.
.PHONY: $(_ITEM_)-html
$(_ITEM_)-html: $($(_DIR_)_DOX_HTML_PATH_)
$(call make-dbg,doxygen tree,\
	"New dependency: $(_ITEM_)-html: $($(_DIR_)_DOX_HTML_PATH_)")

# Building directory should build the item as well.
$(_DIR_BUILD_): $($(_DIR_)_DOX_HTML_PATH_)
$(call make-dbg,doxygen tree,\
	"New dependency: $(_DIR_BUILD_): $($(_DIR_)_DOX_HTML_PATH_)")

# HTML build requires only the 1st pass.
.PHONY: $($(_DIR_)_DOX_HTML_PATH_)
$($(_DIR_)_DOX_HTML_PATH_): .$(_ITEM_ID_)
$(call make-dbg,doxygen tree,\
	"New dependency: $($(_DIR_)_DOX_HTML_PATH_): .$(_ITEM_ID_)")

# Clean generated folder.
_clean_ += $($(_DIR_)_DOX_HTML_PATH_)

endif


# - LaTeX -------------------------------------------------------------------- #
#
# Doxygen generates a simple makefile, but we're going to ignore it and use
# latexmk because we don't care for the reinvention of the wheel and because it
# actually does a better job at managing rebuilds.

ifeq ($($(_DIR_)_DOX_LATEX_),YES)

# Convenience target.
.PHONY: $(_ITEM_)-latex
$(_ITEM_)-latex: $($(_DIR_)_DOX_LATEX_PATH_)/$(_CODE_NAME_).pdf
$(call make-dbg,doxygen tree,\
	"New dependency: $(_ITEM_)-latex: $($(_DIR_)_DOX_LATEX_PATH_)/$(_CODE_NAME_).pdf")

# Building directory should build the item as well.
$(_DIR_BUILD_): $($(_DIR_)_DOX_LATEX_PATH_)/$(_CODE_NAME_).pdf
$(call make-dbg,doxygen tree,\
	"New dependency: $(_DIR_BUILD_): $($(_DIR_)_DOX_LATEX_PATH_)/$(_CODE_NAME_).pdf")

# Actual LaTeX compilation rule.
.PHONY: $($(_DIR_)_DOX_LATEX_PATH_)/$(_CODE_NAME_).pdf
$($(_DIR_)_DOX_LATEX_PATH_)/$(_CODE_NAME_).pdf: .$(_ITEM_ID_)
	$(call echo,"DOX","$@","$(COLOUR_DOX)")
	cd $(@D); latexmk -pdf -f refman.tex $(_DOX_LATEX_QUIET_)
	cp $(@D)/refman.pdf $@

$(call make-dbg,doxygen tree,\
	"New dependency: $($(_DIR_)_DOX_LATEX_PATH_)/$(_CODE_NAME_).pdf: .$(_ITEM_ID_)")

# Clean generated folder.
_clean_ += $($(_DIR_)_DOX_LATEX_PATH_)

endif


# = Extra clean targets ====================================================== #

$(call clean-gen-batch,$(_clean_),$(COLOUR_DOX))

endif
