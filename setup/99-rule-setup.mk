# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Rule setup
#


# = Rule specific setup ====================================================== #
#
# Include any setup from rules that may need it. This allows rules to specify
# one-off initializations or operations that would otherwise be repeated for
# each subdirectory.

# Provide this path to all rules so they can avoid calling a function to get
# their path all the time.
_RULE_DIR_ := $(call dir-fix,$(_MAKEDIR_)/rules)

# Initialise so each rule can append their own interface variables here.
_CLEANUP_ :=

include $(sort $(wildcard $(_MAKEDIR_)/rules/*/setup.mk))


# = Bootstrap the recursive rules ============================================ #
#
# This is where we start praying to the GNU head. May she grant us our wishes!

_RULES_ := $(sort $(wildcard $(_MAKEDIR_)/rules/*.mk))
include dir.mk $(_RULES_)
