# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# OpenOCD related rules
#


ifneq ($(filter openocd,$(_SUBTYPES_) $(_TYPE_)),)
$(call make-dbg,openocd)

ifeq ($(strip $(_TYPE_)),)
$(call make-error,\
	"OpenOCD rule requires an ITEM and TYPE to be defined as a subtype rule. "\
	"Consider using it as a type rule")
endif

# Parse interface variables.
include $(_RULE_DIR_)/openocd/parse.mk


# = Rules ==================================================================== #

# These targets have the item ID appended so that the user may be explicit about
# which item is to be flashed, debugged, etc.
#
# Most of these have an order only dependency as they imply the existence of a
# compiled file, but it would limit the use cases to force a recompile with a
# normal dependency.

.PHONY: openocd-$(_ITEM_ID_)
openocd-$(_ITEM_ID_):
	$($(_item_id_)_OPENOCD_)         \
		-f $($(_item_id_)_LINK_) \
		-f $($(_item_id_)_TARGET_) 2>/dev/null &
	sleep 0.1
	telnet localhost 4444
	echo shutdown | nc -t localhost 4444 || true

.PHONY: halt-$(_ITEM_ID_)
halt-$(_ITEM_ID_):
	$($(_item_id_)_OPENOCD_)           \
		-f $($(_item_id_)_LINK_)   \
		-f $($(_item_id_)_TARGET_) \
		-c "init"                  \
		-c "reset halt"            \
		-c "shutdown"

.PHONY: reset-$(_ITEM_ID_)
reset-$(_ITEM_ID_):
	$($(_item_id_)_OPENOCD_)           \
		-f $($(_item_id_)_LINK_)   \
		-f $($(_item_id_)_TARGET_) \
		-c "init"                  \
		-c "reset run"             \
		-c "shutdown"

.PHONY: flash-$(_ITEM_ID_)
flash-$(_ITEM_ID_): | $($(_ITEM_ID_)_BIN_)
	$($(_item_id_)_OPENOCD_)                                         \
		-f $($(_item_id_)_LINK_)                                 \
		-f $($(_item_id_)_TARGET_)                               \
		-c "init"                                                \
		-c "reset halt"                                          \
		-c "sleep 100"                                           \
		-c "wait_halt 2"                                         \
		-c "flash write_image erase $| $($(_item_id_)_ADDRESS_)" \
		-c "sleep 100"                                           \
		-c "verify_image $| $($(_item_id_)_ADDRESS_)"            \
		-c "sleep 100"                                           \
		-c "reset"                                               \
		-c "shutdown"


# - Debugging ---------------------------------------------------------------- #

.PHONY: dbg-$(_ITEM_ID_)
dbg-$(_ITEM_ID_): | $($(_ITEM_ID_)_ELF_)
	$($(_item_id_)_GDB_) -ex $(_gdb_cmd_) $|

.PHONY: ddd-$(_ITEM_ID_)
ddd-$(_ITEM_ID_): | $($(_ITEM_ID_)_ELF_)
	ddd --eval-command=$(_gdb_cmd_) --debugger $($(_item_id_)_GDB_) $|


# = Commit rules ============================================================= #
#
# Due to how make expands variables within rules, we need to override some
# variables on a target basis.

openocd-$(_ITEM_ID_): _item_id_ := $(_ITEM_ID_)
halt-$(_ITEM_ID_):    _item_id_ := $(_ITEM_ID_)
reset-$(_ITEM_ID_):   _item_id_ := $(_ITEM_ID_)
flash-$(_ITEM_ID_):   _item_id_ := $(_ITEM_ID_)
dbg-$(_ITEM_ID_):     _item_id_ := $(_ITEM_ID_)
ddd-$(_ITEM_ID_):     _item_id_ := $(_ITEM_ID_)

_openocd_cmd_ :=    $($(_ITEM_ID_)_OPENOCD_)
_openocd_cmd_ += -f $($(_ITEM_ID_)_LINK_)
_openocd_cmd_ += -f $($(_ITEM_ID_)_TARGET_)

_gdb_remote_cmd_ := "target remote | $(_openocd_cmd_) -c gdb_port\ pipe"

dbg-$(_ITEM_ID_): _gdb_cmd_ := $(_gdb_remote_cmd_)
ddd-$(_ITEM_ID_): _gdb_cmd_ := $(_gdb_remote_cmd_)

endif
