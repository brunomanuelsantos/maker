.. Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


=====
Maker
=====

Congratulations, you've found yet one more project in the great line of the
dreadfully boring build systems!

This is a generic build system designed to work atop GNU Make. Grown out of
frustration with writing massive makefiles that are only ever good enough for a
handful of projects, and a serious personal issue with the otherwise typical and
simple to write recursive make strategy.

If you get lost, find home at https://gitlab.com/brunomanuelsantos/maker.


Basic usage
===========

This is a complex project and requires some reading for effective usage.
Nonetheless, let's see how to compile the most common program in all history.

Start by locating the build system in your file system and creating a symbolic
link to the ``Makefile`` in your project's root directory. Next create a
``dir.mk`` like so::

   # Select the rule you want to apply and give a name to the end product.
   TYPE = executable
   ITEM = hello

   # List your sources. You know what this one looks like!
   SRC = hello.c

   # Recall a generic template defining CC & Co.
   $(call ccppasm-template, gcc-common)

With this, you can now run ``make BUILDPATH=. hello`` to build ``hello`` in
source. You can also specify a ``project.mk`` as::

   # Specify a default build path.
   BUILDPATH ?= some-path

   # Specify the default target to be built.
   all: hello

And now a ``make`` will suffice :)


Documentation
=============

The compiled documentation is hosted in
https://brunomanuelsantos.gitlab.io/maker/.

The same documentation is also readable in plain text or through GitLab's reST
renderer from the directory ``doc``, but it won't resolve links and cross
references.


Contributing
============

Whatever works best with a strong preference for email based workflow though.


License
=======

This project is licensed under `GPL-v3`_. The license shall *not* cover works or
respective outputs that meet the following conditions though:

#. Works that interact with the build system through the provided interface
   variables only, provided that the combined work is not itself a build system
   itself.

#. Products of compilation or any file manipulation done by the build system on
   behalf of the user, provided the final product is not a build system itself.

This relates specifically to the definition of "covered work" as defined in GPL
v3.0 or similar concept.

.. _GPL-v3: https://www.gnu.org/licenses/gpl-3.0.en.html


Contacts
========

* Mailing list: `maker@freelists.org`_

.. _maker@freelists.org: https://www.freelists.org/list/maker
