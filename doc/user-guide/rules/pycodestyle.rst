.. Copyright (c) 2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


===========
Pycodestyle
===========

.. _pycodestyle-types:

Types
=====

``pycodestyle``

   Defines individual targets to check Python source files for style warnings
   and some errors with |pycodestyle|, formerly known as `PEP8`.

   This rule specifies the targets ``<item>-pycodestyle``,
   ``<item>-pycodestyle-first`` and ``<item>-pycodestyle-stat``. Each equivalent
   to calling the tool with no extra flags, with ``--first`` and with ``-qq
   --statistics`` respectively.


Subtypes
========

``pycodestyle``

   See :ref:`pycodestyle-types`.


Variables
=========

Specific
~~~~~~~~

``STYLE_SRC``

   Files and / or directories on which to call the style checker.

   As a type rule, this variable can be assigned multiple times resulting in a
   list of many files. As a subtype rule the variable is only parsed once, so
   all the source files and directories must be known in the item's root
   directory.

``STYLE_CONF``

   Configuration file to override the tool defaults. Syntax is covered in the
   `pycodestyle documentation`_.

   As a type rule, this variable can be assigned multiple times resulting in a
   list of many files. As a subtype rule the variable is only parsed once, so
   all the configuration files must be known in the item's root directory.

   Defining a configuration file is entirely optional.

.. _`pycodestyle documentation`:
   https://pycodestyle.pycqa.org/en/latest/intro.html#configuration


Debug keys
==========

``pycodestyle``

   This agglomerates all debug information related to |pycodestyle| targets.
