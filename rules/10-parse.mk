# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Parse all the variables from a dir.mk file
#


$(call make-dbg,tree clean)

# = Subdirectories =========================================================== #

_SUBDIRS_ := $(call dir-fix,$(addprefix $(_DIR_)/,$(call dedup,$(SUBDIRS))))


# = Stacks =================================================================== #
#
# Here be dragons!
#
# We keep several 'stacks' which are maintained together. As a result, an entry
# in one of them can always be correlated to an entry in the other. These allow
# us to keep track of the item being built in any given folder, potentially with
# nested items.
#
# The stacks we're interested in are not 'directory stacks', but these are the
# ones that allow us to easily track ancestry. This is why we use them to obtain
# a count of how many elements to pop each iteration.
#
# This is obviously tied to the depth first traversal and the order of entries
# in the stacks.

# - Stack of compilation end products (items) -------------------------------- #
#
# 'End products', or items, are defined by a TYPE *and* an ITEM (name). We'll
# keep those in sync and track which subdirectory they apply to.

_parents_ := $(call parents,$(_DIR_),$(_ITEM_DIR_STACK_))
_pop_n_   := $(shell expr $(words $(_ITEM_DIR_STACK_)) - $(words $(_parents_)))

_ITEM_DIR_STACK_ := $(call pop,$(_pop_n_),$(_ITEM_DIR_STACK_))
_TYPE_STACK_     := $(call pop,$(_pop_n_),$(_TYPE_STACK_))
_ITEM_STACK_     := $(call pop,$(_pop_n_),$(_ITEM_STACK_))

# Test if the user has defined a new item for the current directory. These two
# variables have large repercussions throughout the build system. Correctness
# needs to be guaranteed.
ifeq ($(words $(TYPE) $(ITEM)),2)
ifeq ($(words $(TYPE)),1)
_USR_TYPE_ := $(TYPE)
_USR_ITEM_ := $(ITEM)
endif
else
ifneq ($(words $(TYPE) $(ITEM)),0)
$(call make-error,"Bad ITEM and / or TYPE")
else
_USR_TYPE_ :=
_USR_ITEM_ :=
endif
endif

# If new thingy declared, push root directory onto the stack for the new item.
ifneq ($(strip $(_USR_ITEM_)),)
_ITEM_DIR_STACK_ := $(call push,$(_DIR_),$(_ITEM_DIR_STACK_))
_TYPE_STACK_     := $(call push,$(_USR_TYPE_),$(_TYPE_STACK_))
_ITEM_STACK_     := $(call push,$(_USR_ITEM_),$(_ITEM_STACK_))
endif


$(call make-dbg,tree,"Directory stack: $(strip $(_ITEM_DIR_STACK_))")
$(call make-dbg,tree,"TYPE stack:      $(strip $(_TYPE_STACK_))")
$(call make-dbg,tree,"ITEM stack:      $(strip $(_ITEM_STACK_))")

# Finally, get the *current* item description.
_ITEM_DIR_ := $(firstword $(_ITEM_DIR_STACK_))
_TYPE_     := $(firstword $(_TYPE_STACK_))
_ITEM_     := $(firstword $(_ITEM_STACK_))

# In case items have the same name, instead of preventing it, we'll try and
# limit the damage. Prefixing the directory yields a unique name.
_ITEM_ID_ := $(_ITEM_DIR_)@$(_ITEM_)

# Register any additional types that should apply in this directory.
_SUBTYPES_ := $(SUBTYPES)


# = Declare hierarchical directory dependencies ============================== #
#
# Declare target for current directory. Prerequisite directories will eventually
# receive the same treatment once make traverses them.
#
# Additionally, cleaning this directory shall imply cleaning all
# sub-directories.

# We call dir-fix again to get rid of the ./ for in-source builds. It will still
# leave a '.' for the root.
_SUBDIRS_BUILD_ := $(call dir-fix,$(addprefix $(_BUILDPATH_)/,$(_SUBDIRS_)))

$(_DIR_BUILD_): $(_SUBDIRS_BUILD_)
$(call make-dbg,tree,"New dependency: $(_DIR_BUILD_): $(_SUBDIRS_BUILD_)")

$(call clean-gen-batch,$(_SUBDIRS_BUILD_),$(COLOUR_DEFAULT),empty)
$(call distc-gen-batch,$(_SUBDIRS_BUILD_),$(COLOUR_DEFAULT),empty)
