.. Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


============
Introduction
============

.. toctree::
   :maxdepth: 3

   abstract
   motivation
