# Copyright (c) 2018-2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later

SUBDIRS = extra

TYPE = executable
ITEM = main

$(call ccppasm-template,cross-cortex-m4)

SRC = main.c

CFLAGS  += -ffreestanding
LDFLAGS += -ffreestanding -nostdlib

# Sadly 'freestanding' doesn't get rid of all the non standard headers, it just
# disables some of the functionality, but keeps all the headers in the search
# path. This ensures the real thing, by the book.
_gcc_dir_ := $(shell \
	$(CROSS)$(CC) -print-search-dirs | sed -n 's/install: \(.*\)\//\1/p')

CFLAGS += -nostdinc -isystem $(_gcc_dir_)/include
LIBS   += -lgcc -L $(call dirmk-dir)/extra -T stm32f407vgt6.ld

# Force an OpenOCD to run as well. This will fail if no suitable target is
# available, but it allows testing that OpenOCD rules are also issued correctly.
$(call build-dir): flash-$(call dirmk-dir)@$(ITEM)
