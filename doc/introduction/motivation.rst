.. Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


==========
Motivation
==========

|make| has been around for a long time and some nice systems have been appearing
to replace it. Some of these may already bring us closer to meeting all the
objectives in superior ways, yet they are fairly new and far from being the
'gold standard' that |make| has been since 1976 (I checked). This is also
perhaps intended for a slightly different audience.

In spite of the many technical issues with |make|, none is worse than the issues
arising from bad usage, sometimes as recommended. These result in two
independent problems that are often bundled together:

#. Incorrect builds.
#. Terrible performance.

The following sections detail some of the pitfalls we're trying to avoid as well
as some of the idealized goals we want to achieve.


Correctness
===========

In-source compilation
~~~~~~~~~~~~~~~~~~~~~

This traditional way of building things is not only a severe limitation as it
induces full recompilation cycles when (e.g.) compiling a debug build.

Failure to trigger a full recompile can also lead to products of different
compilation runs being used in a single final output with often obscure results.

This can be addressed by supporting out of source compilations, yet the common
way of achieving it (``VPATH``) has severe drawbacks as detailed in
:ref:`why-not-vpath`. This build system needs a different approach then.


Compilation products left behind
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Deleting a source file doesn't mean that the respective output file is deleted.
|make| has no feature to help here, and adding it to make is impractical. Even
systems that do account for this (e.g. `Tup <http://gittup.org/tup/>`_
[#tupnote]_) have a range of usability issues that are usually deemed too dear.

Namely, this implies a file system monitor and a database being managed on the
background. We want to keep our build system stateless though.

Still, not all is lost, this can be mitigated at least in some types of rules by
not automatically discovering files to operate on. Instead, by facilitating and
promoting that the user shall list all sources by name, the build system can
guarantee that the extra files are nothing but noise on the build directory.


Missing dependencies
~~~~~~~~~~~~~~~~~~~~

Writing dependencies gets very tedious and in some cases it's not obviously
they're missing. A typical example is a library being compiled together with the
user of the library in the same project and yet a change in the library going
unnoticed by the linker rule for the library user.

This is simply poor makefile writing and not a fault of any build system.
However, we can declare as many dependencies automatically as possible so that
the user can focus on the few that really need human reasoning.

.. note::

   Dependencies on makefiles themselves are not considered. This would be easy
   enough, leading to a total accuracy, but doing so would trigger a recompile
   of potentially large portions of a project by simply introducing a new source
   file nested deep into its source tree, defeating the purpose of the build
   system.


Spaghetti Make
~~~~~~~~~~~~~~

There's no real solution for this one aside from using something other than
|make|. Short of that, a trade off is needed between pretty makefiles and
powerful / performant ones. We're aiming at the latter. With that said, the user
makefiles should be pretty and simple, which makes it OK.

Choosing to use a non recursive make adds to the problem as effectively we need
to define a complex multiple file build system where every single variable is
global.


Performance
===========

Recursive Make invocations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

By far the biggest issue and something that is recommended and replicated
ubiquitously in many projects. Writing a recursive |make| based build system is
easy and even elegant from an implementation stand point.

Since all variables are global per invocation, this limits the scope of
variables to a single directory, unless the developer chooses otherwise. Rules
can also make use of ``VPATH`` with some possible improvements.

And yet this is the root of all evil when it comes to |make|. A single build
will either do too much or too little. Furthermore, |make| will often build the
dependency DAG twice before doing any work and it will take a long time even to
decide there's *nothing* to be done.

A great reference on this is Petter Miller's paper
`Recursive Make Considered Harmful (1998)
<https://duckduckgo.com/?q=recursive+make+considered+harmful&ia=web>`_.

Recursive |make| has some desirable features though. In particular, building sub
trees of the project becomes almost trivial to achieve or even single files.
This is something we want to retain and, as it turns out, quite compatible with
other goals.


1976 gremlins
~~~~~~~~~~~~~

|make| is old and hidden assumptions and recipes about how you should use |make|
are a dime a dozen.

These options are almost never used in the wild, but also always enabled by
default. Loading these are usually not an issue, but they are a waste of time
--- a small one for non-recursive implementations, but a waste nonetheless.

The idea of this build system is that it provides a reusable core of makefiles
that offset the amount of effort needed in the user application, but disabling
all the native |make| recipes which suffer from a severe case of overreach.


Usability
=========

Build systems are expected to "make the easy things trivial and the difficult
ones possible".

A telling example of how this philosophy was put in practice --- and for some
weird reason replicated even in newer systems ---, is the prevalence of so
called "implicit rules" which are invariably only useful for computer
programming 101 exercises.

The user is then expected, not only to know these rules by heart, but to know
how these rules apply as well. As an example, |make|'s rule precedence is
described `here
<https://www.gnu.org/software/make/manual/html_node/Implicit-Rule-Search.html#Implicit-Rule-Search>`_.
So much for making easy things trivial.

This build system needs to make the difficult things trivial and the easy ones
as arbitrarily complicated as necessary. It does define its own rules, no doubt,
but the selection and configuration criteria shall be more powerful and easy to
arbitrate by requiring that the user writes simple, yet meaningful instructions
that univocally select the applicable rule.


.. rubric:: Footnotes

.. [#tupnote]

   Tup is a build system that solves the correctness issue to a fault, depending
   on the opinion. In any case, the white paper provides a very good crash
   course on build systems' issues.
