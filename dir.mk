# Copyright (c) 2018-2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later

SUBDIRS = doc tests

_TEST_BUILDDIR := $(call build-path)/tests

# By default, don't sort unless building in parallel.
_SORT ?= $(if $(filter -j1,$(MAKEFLAGS)),tee,sort)
_DIFF ?= diff -u

.PHONY: run-tests
run-tests:
	$(call echo,"TEST",,"$(COLOUR_BOLD)$(COLOUR_RED)")
	$(MAKE) -C tests COLOUR=no       -Bn  2>&1 | $(_SORT) | $(_DIFF) $(_TEST_BUILDDIR)/cmd-build.stdout -
	$(MAKE) -C tests COLOUR=no       -Bik 2>&1 | $(_SORT) | $(_DIFF) $(_TEST_BUILDDIR)/build.stdout     -
	$(MAKE) -C tests COLOUR=no clean -n   2>&1 | $(_SORT) | $(_DIFF) $(_TEST_BUILDDIR)/cmd-clean.stdout -

.PHONY: update-tests-output
update-tests-output:
	$(call echo,"GEN OUTPUT",,"$(COLOUR_BOLD)$(COLOUR_RED)")
	mkdir -p $(_TEST_BUILDDIR)
	$(MAKE) -C tests COLOUR=no       -Bn  2>&1 | $(_SORT) > $(_TEST_BUILDDIR)/cmd-build.stdout
	$(MAKE) -C tests COLOUR=no       -Bik 2>&1 | $(_SORT) > $(_TEST_BUILDDIR)/build.stdout
	$(MAKE) -C tests COLOUR=no clean -n   2>&1 | $(_SORT) > $(_TEST_BUILDDIR)/cmd-clean.stdout

# Clean the expected output only on a distclean.
_distc_ := cmd-build build cmd-clean
_distc_ := $(addsuffix .stdout, $(_distc_))
_distc_ := $(addprefix $(_TEST_BUILDDIR)/, $(_distc_))
$(call distc-gen-batch,$(_distc_),$(COLOUR_CC))
