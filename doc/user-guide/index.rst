.. Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


==========
User guide
==========

.. toctree::
   :maxdepth: 3

   usage
   interface-variables

   rules/index
