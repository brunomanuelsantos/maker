/*
 * Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "archive/lib.h"
#include "static/lib.h"
#include "../good-c-shared/lib.h"

#include "extra/extra.h"

int main(void)
{
	extra_fun();
	archive_fun();
	visible_static_fun();
	visible_shared_fun();

	return 0;
}
