.. Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


===========================
Generic interface variables
===========================

Variables are a tricky business in Make. Understanding the issues documented in
the section :ref:`variables-conventions` should be considered a pre-requisite
for proper usage of this build system.


Project variables
=================

These are only to be specified in the command line, environment or in
``project.mk``. Setting them anywhere else will be silently ignored. Note also
these are unique to the project and not subdirectories. No rule can extend this
set.

``BUILDPATH``

   This variable controls the build path. If this is the root of the project
   being compiled, the build system will place the outputs alongside the
   sources. If a different path, the compiled outputs will be placed in a
   directory tree that mimics the source tree with its root in the path
   specified by this variable.

``VERBOSITY``

   It defaults to ``0`` and controls the verbosity of the build process. The
   higher the value, the more output should be put out.

   Note that the output is not synchronized by default either due to breaking
   terminal interactive programs that may be part of a rule. These are a bit of
   an exception, but still should be trivial for the user to specify ``-O`` or
   ``-j1`` if / when interested in parsing any such output.

   Actual effects of this variable are specific to each rule set, although most
   ones should have no need for the feature.

``DEBUG``

   This defaults to ``0``, meaning 'production' build. Other values enable
   increasing levels of debugging features with the specific effects depending
   on the specific rules being used, possibly having no effect at all.

   For out of source builds, enabling this option will redirect the compilation
   results to a separate folder with the ``-dbgN`` suffix, where ``N`` is the
   value of ``DEBUG``. However, for in-source builds, no full recompilation will
   be triggered on account of changing this variable.

``MAKEDBG``

   This variable controls the ``make-dbg`` function output. It should be passed
   a list of keys for which to enable debug prints. This list can also make use
   of the pattern matching ``%`` to match multiple keys according to the
   semantics of |make|'s ``filter`` function.

   The global keys are documented in section :ref:`debug-keys`. Each rule may
   specify additional keys however, typically one per rule and with the same
   name as the rule itself.

``COLOUR``

   Whether to enable coloured output or not. It can be ``no`` to disable, all
   other values (including not being defined) count as ``yes``.

``COLOUR_DEFAULT``, ``COLOUR_MAKEDBG``, ``COLOUR_MAKEERROR``

   Colour codes for formatting the output of the build system. Additional
   ``COLOUR_*`` variables may be defined by specific rules. E.g. ``COLOUR_CC``
   denotes the colour code used for all the ``CC`` lines that would show up on
   as a result of using the C compiler in this case. Each rule should document
   their own.

   These variables are not cached and are simply expanded. This means that the
   last assignment is the one used everywhere.

``RESET``, ``BOLD``, ``DIM``, ``EMPH``, ``UNDERLINE``, ``HIGHLIGHT``, ``RED``, ``ON_RED``, etc.

   Like the ``COLOUR_*`` variables, these add some colour to the output, but not
   directly. In fact, all ``COLOUR_*`` variables are defined through these, but
   these are never used directly on the print statements.

   While these can be changed, typically these are more useful in overriding the
   other colours: e.g. ``COLOUR_DEFAULT = $(RED)``.

   The red is not a special case however and all following colours have
   equivalent variables: green, yellow, blue, purple, cyan, white and black.


Build variables
===============

These are the short lived variables used to describe what's being built. In
fact, all of these variables are emptied after all the rules have been applied
to a given ``dir.mk`` file to avoid the user having to do so manually in each
``dir.mk`` file.

It's important to note that all directories and source files should be declared
with relative paths. Moreover, it is recommended that each ``dir.mk`` only lists
directories and sources within its own directory.

.. warning::

   Listing sources from other directories despite the indications against it,
   must be done with paths relative to the ``dir.mk`` file in question. It's
   also necessary to keep in mind inclusion order of sub-subdirectories, which
   could introduce very obscure and latent bugs. Needless to say that the fact
   that this works at all should be viewed as both a coincidence --- it was not
   designed ---, and a theoretical capability --- it was not tested.


Control variables
~~~~~~~~~~~~~~~~~

These are crucial for defining what is being compiled and apply universally
regardless of the specific compilation rules. I.e. they're equally relevant when
instructing the compilation of a *C* program as when compiling a *LaTeX*
document.

An important concept here is that these variables are *hierarchically
overridden*. I.e. each time they're defined, they signal the build system to
treat that folder, not as part of whatever was being built up to then, but a
different item altogether. This way, the user may define nested items, each with
a separate or the same rule being applied.

All control variables are only parsed in the root of the respective item. The
global ones are actively checked as they can cause serious bugs, and will
trigger errors if not used correctly. Whether or not specific rules throw errors
on their control variables is left up to them.

``ITEM``

   This is the name used for the final object that gets built. This should be
   defined *if and only if* ``TYPE`` is defined in the same directory.

``TYPE``

   This defines what kind of object the build system should build for a given
   item. The dynamic rules depend on this variable to know whether they're
   applicable or not. This should be defined *if and only if* ``ITEM`` is
   defined in the same directory.

``SUBTYPES``

  List of subtypes to enable for the current directory. Unlike those defined by
  ``TYPE``, there can be multiple subtypes per directory.


Source variables
~~~~~~~~~~~~~~~~

Source variables are used to indicate all the sources that are to be built.
These variables are immediately expanded regardless of how the user declares
them and erased by the build system before entering another directory.

Each set of rules may make use of different sets of interface variables.
Different rules may use the same interface variables or not.

The global ones belonging to the core of the build system itself are:

``SUBDIRS``

   This a list of subdirectories containing further ``dir.mk`` files. The
   inclusion order is maintained and duplicates are automatically removed.

   Non existing makefiles in a given subdirectory are treated as a bug in the
   user makefile and will trigger an error event.


.. _debug-keys:

Debug keys
==========

These are global debug keys used in the core of the build system. Each rule may
and should define these keys as appropriate together with their own. If all
rules follow this advice, the user could filter on the global key or only on a
rule specific one, thus filtering the output considerably.

The keys are additive though, so if more than one key is specified, the user
will see a combination of all the relevant debug messages, not their
intersection.

The build system core defines the following keys:

``tree``

   This allows the inspection of the directory tree traversal. It also shows the
   automatic hierarchical dependencies generated for each directory.

``clean``

   This allows the inspection of the generation of clean targets. Most of these
   targets are generated automatically as well as their dependencies and this
   allows us to see both as they happen.
