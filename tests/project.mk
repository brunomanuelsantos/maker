# Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later

PROJECT_NAME = Maker tests
CODE_NAME    = Find all the bugs!
VERSION      = $(shell git describe --tags --always)

# For extra credit, build in the directory used by the main project by default.
BUILDPATH ?= ../build/tests

all: $(shell realpath -m -z -s --relative-base=./ $(BUILDPATH))
