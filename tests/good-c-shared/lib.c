/*
 * Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

void hidden_shared_fun(void)
{
}

EXPORT void visible_shared_fun(void)
{
}
