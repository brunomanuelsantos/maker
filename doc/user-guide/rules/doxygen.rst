.. Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


=======
Doxygen
=======

.. _doxygen-types:

Types
=====

``doxygen``

   Defines individual targets to compile |html| and |latex| documents. This rule
   provides a template for the configuration so the user can specify a minimum
   set of options.


Subtypes
========

``doxygen``

   See :ref:`doxygen-types`.


Variables
=========

Generic
~~~~~~~

``VERBOSITY``

   The tools used by this rule are quite verbose, by specifying ``0`` (default),
   output is hidden from both |doxygen| and |latex| compilation. |doxygen|
   warnings and errors will still be shown though.

   The user may specify two levels: ``0`` (default) and ``1``. Values above
   ``1`` have the same effect as ``1``.


Specific
~~~~~~~~

``CONF``

   Configuration file to extend the default configuration. Any option set within
   this file takes precedence over the build system defaults.


Debug keys
==========

``doxygen``

   This agglomerates all debug information related to |doxygen| targets.
