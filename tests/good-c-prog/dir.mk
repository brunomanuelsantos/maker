# Copyright (c) 2018-2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later

SUBDIRS = archive static extra

TYPE = executable
ITEM = main

$(call ccppasm-template,native-x86-64)

SRC = main.c

CFLAGS += -O2

_build_dir_  := $(call build-dir)
_extlib_dir_ := $(call dir-fix,$(_build_dir_)/..)

$(_build_dir_)/$(ITEM): _build_dir_  := $(_build_dir_)
$(_build_dir_)/$(ITEM): _extlib_dir_ := $(_extlib_dir_)
$(_build_dir_)/$(ITEM):                     \
	$(_build_dir_)/static/libstatic.o   \
	$(_build_dir_)/archive/libarchive.a \
	$(_extlib_dir_)/good-c-shared/libshared.so
