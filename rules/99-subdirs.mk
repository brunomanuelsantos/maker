# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Rules to walk the directory recursively
#
# This rule has 2 purposes:
# - Source all subdirectories' dir.mk files.
# - Source all make rules for each dir.mk file.
#


$(call make-dbg,tree)

# = Recursively add subdirectories =========================================== #
#
# Directory traversal is done depth-first interleaving each dir.mk file with all
# the rule files specified by _RULES_.

_dirfiles_ := $(addsuffix /dir.mk,$(_SUBDIRS_))
$(call make-dbg,tree,"Extra dir.mk files: $(strip $(_dirfiles_))")


# - Include all rules / variables from each sub directory -------------------- #
#
# Interleave each dir.mk file with all the rules. The source tree traversing is
# therefore depth-first, which is crucial for how we parse dir.mk files.

include $(foreach i,$(_dirfiles_),$i $(_RULES_))
