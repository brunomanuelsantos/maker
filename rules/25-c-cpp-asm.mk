# Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# C/C++/asm rules
#


ifneq ($(filter executable shared-library static-library archive,$(_TYPE_)),)
$(call make-dbg,executable shared-library static-library archive tree clean)

include $(_RULE_DIR_)/c-cpp-asm/parse.mk

# Initialize variables to track clean and distclean targets to be generated.
_clean_     :=
_distclean_ :=


# = Source rules ============================================================= #

# Directory dependencies.
$(_DIR_BUILD_): $($(_DIR_)_OBJ_)
$(call make-dbg,executable shared-library static-library archive tree,\
	"New dependency: $(_DIR_BUILD_): $($(_DIR_)_OBJ_)")

# Targets for each object file.
_clean_ += $($(_DIR_)_OBJ_)
$($(_DIR_)_OBJ_):

# Targets for each disassembly file.
_clean_ += $($(_DIR_)_ASM_)
$($(_DIR_)_ASM_):

# Include all existing dependency files.
_distclean_ += $($(_DIR_)_DEPS_)
include $($(_DIR_)_DEPS_)


# = Item rules =============================================================== #
#
# Only the root folder of an item should link the item, so we test a relevant
# user variable.
#
# The main targets here are the ones producing an actual linked / archived file.
# These may need to depend on other targets such as directories, which is why
# they all share a pattern in which only *.o, *.a, *.so, *.so.* files are
# actually passed to the linker.

ifneq ($(strip $(_USR_ITEM_)),)

# - Executable --------------------------------------------------------------- #

ifeq ($(strip $(_TYPE_)),executable)

_full_name_ := $(_DIR_BUILD_)/$(_ITEM_)
$(call make-dbg,executable,"Executable: $(_full_name_)")

# Link executable.
$(_full_name_): $$($(_ITEM_ID_)_OBJ_)
	$(call echo,"LD","$@","$(COLOUR_CC)")
	mkdir -p $(@D)
	$($(_item_id_)_BACKEND_) $($(_item_id_)_LDFLAGS_) -o $@ $^ $($(_item_id_)_LIBS_)

# Generate binary file.
$(_full_name_).bin: $(_full_name_)
	$(call echo,"BIN","$@","$(COLOUR_CC)")
	mkdir -p $(@D)
	$($(_item_id_)_OBJCOPY_) -O binary $< $@

# Tentatively set OpenOCD subtype interface variables on behalf of the user.
ELF ?= $(_full_name_)
BIN ?= $(_full_name_).bin

endif


# - Shared library ----------------------------------------------------------- #

ifeq ($(strip $(_TYPE_)),shared-library)

_full_name_ := $(_DIR_BUILD_)/lib$(_ITEM_).so
$(call make-dbg,shared-library,"Shared library: $(_full_name_)")

# Extra compile flags.
$(_ITEM_ID_)_CFLAGS_   += $(_SHARED_LIB_CFLAGS_)
$(_ITEM_ID_)_CXXFLAGS_ += $(_SHARED_LIB_CXXFLAGS_)

# Link library.
$(_full_name_): $$($(_ITEM_ID_)_OBJ_)
	$(call echo,"LD","$@","$(COLOUR_CC)")
	mkdir -p $(@D)
	$($(_item_id_)_BACKEND_) -shared $($(_item_id_)_LDFLAGS_) -o $@	$^ $($(_item_id_)_LIBS_)

endif


# - Static library ----------------------------------------------------------- #

ifeq ($(strip $(_TYPE_)),static-library)

_full_name_ := $(_DIR_BUILD_)/lib$(_ITEM_).o
$(call make-dbg,static-library,"Shared library: $(_full_name_)")

$(_ITEM_ID_)_CFLAGS_   += $(_STATIC_LIB_CFLAGS_)
$(_ITEM_ID_)_CXXFLAGS_ += $(_STATIC_LIB_CXXFLAGS_)

# Link library.
$(_full_name_): $$($(_ITEM_ID_)_OBJ_)
	$(call echo,"LD","$@","$(COLOUR_CC)")
	mkdir -p $(@D)
	$($(_item_id_)_BACKEND_) -nostdlib $($(_item_id_)_LDFLAGS_) -r -o $@ $^ $($(_item_id_)_LIBS_)
	$($(_item_id_)_OBJCOPY_) --localize-hidden $@

endif


# - Archive ------------------------------------------------------------------ #

ifeq ($(strip $(_TYPE_)),archive)

_full_name_ := $(_DIR_BUILD_)/lib$(_ITEM_).a
$(call make-dbg,archive,"Shared library: $(_full_name_)")

# Build archive.
$(_full_name_): $$($(_ITEM_ID_)_OBJ_)
	$(call echo,"AR","$@","$(COLOUR_CC)")
	mkdir -p $(@D)
	$($(_item_id_)_AR_) rcs $@ $^

endif


# - Common targets ----------------------------------------------------------- #

_output_name_ := $(_full_name_)
ifeq ($(strip $(_TYPE_)),executable)
ifeq ($(strip $($(_ITEM_ID_)_BIN_FORMAT_)),true)
_output_name_ := $(_full_name_).bin
endif
endif

# Item disassembly target.
_clean_ += $(_DIR_BUILD_)/$(_ITEM_).asm
$(_DIR_BUILD_)/$(_ITEM_).asm:

# All items share the same dependency signature and all must be added to the
# clean list.
_clean_ += $(call dedup,$(_full_name_) $(_output_name_))
$(call make-dbg,executable shared-library static-library archive tree,\
	"New dependency: $(_full_name_): <all item's .o files; can't inspect due to 2nd expansion>")


# - New dependencies --------------------------------------------------------- #

# Add item as a prerequisite of the directory.
$(_DIR_BUILD_): $(_output_name_)
$(call make-dbg,executable shared-library static-library archive tree,\
	"New dependency: $(_DIR_BUILD_): $(_output_name_)")

# Convenience item target.
.PHONY: $(_ITEM_)
$(_ITEM_): $(_output_name_)
$(call make-dbg,executable shared-library static-library archive tree,\
	"New dependency: $(_ITEM_): $(_output_name_)")


# - Commit rules to item's variables ----------------------------------------- #
#
# Tie current item directory outputs to a given item. This will allow the rules
# to reference item specific variables as, e.g. $($(_item_)_CC_).

$(_DIR_BUILD_)/%: _item_id_ := $(_ITEM_ID_)
$(call make-dbg,executable shared-library static-library archive,\
	"Node $(_DIR_BUILD_) committed to item ID: $(_ITEM_ID_)")

endif


# = Extra clean targets ====================================================== #

$(call clean-gen-batch,$(_clean_),$(COLOUR_CC))
$(call distc-gen-batch,$(_distclean_),$(COLOUR_CC))

endif
