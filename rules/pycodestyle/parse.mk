# Copyright (c) 2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0


# = Parse user variables ===================================================== #

ifneq ($(strip $(_USR_ITEM_)),)
$(_ITEM_ID_)_STYLE_CONF_ :=
$(_ITEM_ID_)_STYLE_SRC_  :=
$(_ITEM_ID_)_BUILDDIR_   := $(_DIR_BUILD_)
endif

$(_ITEM_ID_)_STYLE_SRC_  += $(addsuffix ",$(addprefix "$(_DIR_)/,$(STYLE_SRC)))
$(_ITEM_ID_)_STYLE_CONF_ += $(call abspath,$(addprefix $(_DIR_)/,$(STYLE_CONF)))
