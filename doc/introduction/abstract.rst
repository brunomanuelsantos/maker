.. Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


========
Abstract
========

This is a generic build system designed to work atop GNU Make. Grown out of
frustration with writing massive makefiles that are only ever good enough for a
handful of projects, and a serious personal issue with the otherwise typical and
simple to write recursive make strategy.
