.. Copyright (c) 2018-2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


======
Sphinx
======

.. _sphinx-types:

Types
=====

``sphinx``

   Defines individual targets to compile |html| and |latex| documents among
   others. This rule provides a template for the configuration so the user can
   specify a minimum set of options.

   This defines targets for the |sphinx| builders ``linkcheck``, ``html``,
   ``dirhtml``, ``singlehtml``, ``man``, ``latex`` and ``latexpdf``.


Subtypes
========

``sphinx``

   See :ref:`sphinx-types`.


Variables
=========

Generic
~~~~~~~

``VERBOSITY``

   The tools used by this rule are quite verbose, by specifying ``0`` (default),
   output is hidden from both |sphinx| and |latex| compilation. Since |sphinx|
   doesn't output different types of messages to different files, output is
   either fully silenced or fully enabled.

   The user may specify two levels: ``0`` (default) and ``1``. Values above ``1``
   have the same effect as ``1``.


Specific
~~~~~~~~

``SPHINX_BUILDERS``

   List of |sphinx| builders to generate rules for. By default this enables the
   builders ``dummy``, ``linkcheck``, ``html``, ``dirhtml``, ``singlehtml``,
   ``man``, ``latex`` and ``latexpdf``. The ``latex`` and ``latexpdf`` rules are
   special in that one implies or depends on the other and both rules will be
   generated as long as at least one of them is explicitly named.

``DOC_DIR``

   Source directory for the documentation. This is set by default to the item's
   directory, but may be overridden. This can only be overridden from the
   makefile that specifies the rule (sub)type.

``DOC_CONF``

   Configuration file(s) to extend the default configuration. Any option set
   within this file takes precedence over the build system defaults.

   As a type rule, this variable can be assigned multiple times resulting in a
   list of many files. As a subtype rule the variable is only parsed once, so
   all the configuration files must be known in the item's root directory.

   Defining additional configuration files is entirely optional. The build
   system provides decent defaults without any.


Debug keys
==========

``sphinx``

   This agglomerates all debug information related to |sphinx| targets.
