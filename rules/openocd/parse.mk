# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# User makefiles parser
#


# = Parse interface variables ================================================ #

# Catch some obvious errors.
ifeq ($(strip $(BIN)),)
$(call make-error,"BIN not defined")
endif
ifeq ($(strip $(LINK)),)
$(call make-error,"LINK not defined")
endif
ifeq ($(strip $(TARGET)),)
$(call make-error,"TARGET not defined")
endif
ifeq ($(strip $(ADDRESS)),)
$(call make-error,"ADDRESS not defined")
endif

# Defaults.
CROSS   ?=
GDB     ?= gdb
OPENOCD ?= openocd -s /usr/share/openocd/scripts
ELF     ?=
BIN     ?=
LINK    ?=
TARGET  ?=
ADDRESS ?=


# - File paths --------------------------------------------------------------- #
#
# If defined by the user, it is expected that variables have no path or that the
# path is relative. However, if used as a subtype, the other rule may want to
# direct it to a different file and path cannot be assumed to be relative to the
# user makefile.

_dir_ := $(if $(filter openocd,$(_TYPE_)),$(call dirmk-dir)/,)

BIN := $(_dir_)$(BIN)
ELF := $(_dir_)$(ELF)


# = Register item's variables ================================================ #

# Register item's variables.
$(_ITEM_ID_)_GDB_     := $(CROSS)$(GDB)
$(_ITEM_ID_)_OPENOCD_ := $(OPENOCD)
$(_ITEM_ID_)_ELF_     := $(ELF)
$(_ITEM_ID_)_BIN_     := $(BIN)
$(_ITEM_ID_)_LINK_    := $(LINK)
$(_ITEM_ID_)_TARGET_  := $(TARGET)
$(_ITEM_ID_)_ADDRESS_ := $(ADDRESS)
