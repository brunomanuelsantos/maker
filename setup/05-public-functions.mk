# Copyright (c) 2018-2022 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Auxiliary functions and macros
#


# = Directory resolving ====================================================== #

# - Absolute path ------------------------------------------------------------ #
#
# Resolve a list of paths as canonical absolute paths. The paths need not exist.

abspath = $(strip $(foreach i,$1,$(shell realpath -m -z -s $i)))


# - Resolve dir.mk ----------------------------------------------------------- #
#
# These functions extracts the last included dir.mk file and its path.

# Return the dirmk with a relative path if possible.
dirmk = $(call dir-fix,$(lastword $(filter %dir.mk,$(MAKEFILE_LIST))))

# Return a dirmk directory. This must never evaluate empty, i.e. at least a '.'
# or './' must be returned every time.
dirmk-dir = $(call dir-fix,$(dir $(lastword $(filter %dir.mk,$(MAKEFILE_LIST)))))


# - Resolve build path ------------------------------------------------------- #
#
# This should be totally useless for the build system as direct access to
# _BUILDPATH_ is preferable. It is important for the user however. This is the
# only _good_ way of referencing the build directory from user makefiles.

build-path = $(_BUILDPATH_)
build-dir  = $(_BUILDPATH_)/$(_DIR_)


# = Variable foo ============================================================= #

# - Resolve name of rule variable -------------------------------------------- #
#
# Return the given variable name as the (would be) rule variable name.
#
# Since this may be used at the root of an item's declaration, _ITEM_ID_ may not
# be populated yet or even hold a wrong item ID. For this reason we test and
# resolve the real ID if needed.

rule-var = $(if $(ITEM),$(call dirmk-dir)/$(ITEM),$(_ITEM_ID_))_$1_


# = String manipulation ====================================================== #

# Format string in code names (foo-bar) and variable names (FOO_BAR).
code-name = $(shell echo "$1" | tr '[:upper:]' '[:lower:]' | tr _ - | tr " " -)
var-name  = $(shell echo "$1" | tr '[:lower:]' '[:upper:]' | tr - _ | tr " " _)

# Complements to filter and filter-out capable of finding matches of a single
# sequence $1 in any element of $2.
containing     = $(foreach v,$2,$(if $(findstring $1,$v),$v,))
not-containing = $(foreach v,$2,$(if $(findstring $1,$v),,$v))


# = Print macros ============================================================= #

# - Makefile debug print ----------------------------------------------------- #
#
# Nifty little function to print debug messages. This has no effect and is
# virtually performance neutral unless MAKEDBG contains a matching key.
#
# E.g., to debug elements marked with the key 'tree', the user may call:
#   make target MAKEDBG=tree
#
# The intended usage is simply:
#   $(call make-dbg,keys,"arbitrary message")
#
# Note that each print may match several keys and multiple keys may be specified
# as well.
#
# If no second argument is specified, a standardized message is printed with the
# name of the user makefile being processed instead.

ifneq ($(strip $(MAKEDBG)),)
make-dbg = $(if $(filter $1,$(MAKEDBG)), $(warning $(shell printf "\t%b%b"	\
	$(if $2,"    "$2,"$(COLOUR_MAKEDBG)<$(call dirmk)>") "$(COLOUR_RESET)")))
else
make-dbg =
endif


# - Error wrapper ------------------------------------------------------------ #

make-error = $(error $(shell printf "%b%s: %s%b" \
	"$(COLOUR_MAKEERROR)" "$(dirmk)" $1 "$(COLOUR_RESET)"))


# - Abbreviated command print ------------------------------------------------ #
#
# This takes 3 parameters: 2 strings and a colour escape sequence that applies
# only to the first of the other two strings. The intended usage is:
#
# target:
# 	$(call echo,"CC","$(FILE)","$(COLOUR_RED)")
#
# Where only CC is coloured according to the selected colour code.

_ECHO_ALIGN_ = 15
echo = +printf "%b  %-$(_ECHO_ALIGN_)s%b%s\n" $3 $1 "$(COLOUR_RESET)" $2
