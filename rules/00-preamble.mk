# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Setup a common set of variables for correct parsing of all the rules
#

$(call make-dbg,tree)


# = Resolve the relative path of the dir.mk file ============================= #
#
# Extract all previous dir.mk files. The relative path of our current directory
# can be extracted from the last of these entries.

_DIR_       := $(call dirmk-dir)
_DIR_BUILD_ := $(call dir-fix,$(_BUILDPATH_)/$(_DIR_))

$(call make-dbg,tree,"Evaluating directory: $(_DIR_)")
