# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Parse user variables
#


# = Resolve user overrides =================================================== #
#
# The user must specify a Doxyfile of his own. Get it for inclusion in the one
# included in the build system.

ifeq ($(strip $(CONF)),)
$(call make-error,"Doxygen rule missing local Doxyfile (CONF)")
endif
$(_DIR_)_CONF_ := $(_DIR_)/$(CONF)


# - Paths -------------------------------------------------------------------- #
#
# The user may override the normal paths for doxygen output and may enable
# different options through their local Doxyfile. We really need these paths...
#
# However, we won't allow the user to completely screw thing over here. Even if
# he did specify directories, these will always be relative to the current
# directory's build path.

# First we state the defaults, in case we don't read anything else.
$(_DIR_)_DOX_PATH_       := $(_DIR_BUILD_)
$(_DIR_)_DOX_HTML_PATH_  := $(_DIR_BUILD_)/html
$(_DIR_)_DOX_LATEX_PATH_ := $(_DIR_BUILD_)/latex

# Now we take the user overrides, if any.
_prefix_ :=
_html_   :=
_latex_  :=
ifneq ($($(_DIR_)_CONF_),)
_prefix_ := $(shell grep -oP 'OUTPUT_DIRECTORY\s*=\s*\K.*' < $($(_DIR_)_CONF_))
_html_   := $(shell grep -oP 'HTML_OUTPUT\s*=\s*\K.*'      < $($(_DIR_)_CONF_))
_latex_  := $(shell grep -oP 'LATEX_OUTPUT\s*=\s*\K.*'     < $($(_DIR_)_CONF_))
endif

# If the root path was changed, we assume this is it and we prefix all other
# relative paths with it.
ifneq ($(strip $(_prefix_)),)
$(_DIR_)_DOX_PATH_       := $(call dir-fix,$(_DIR_BUILD_)/$(_prefix_))
$(_DIR_)_DOX_HTML_PATH_  := $($(_DIR_)_DOX_PATH_)/ltml
$(_DIR_)_DOX_LATEX_PATH_ := $($(_DIR_)_DOX_PATH_)/latex
endif

# And finally we override specific paths if the user has specified so.
ifneq ($(strip $(_html_)),)
$(_DIR_)_DOX_HTML_PATH_  := $(call dir-fix,$($(_DIR_)_DOX_PATH_)/$(_html_))
endif
ifneq ($(strip $(_latex_)),)
$(_DIR_)_DOX_LATEX_PATH_ := $(call dir-fix,$($(_DIR_)_DOX_PATH_)/$(_latex_))
endif


# - What's being built? ------------------------------------------------------ #
#
# Again, with the user specifying things outside the makefiles through the
# Doxyfile, we have to get clever if we hope to be of any use over just calling
# Doxygen.

ifneq ($($(_DIR_)_CONF_),)
_html_enabled_  := $(shell grep -oP 'GENERATE_HTML\s*=\s*\K.*'  < $($(_DIR_)_CONF_))
_latex_enabled_ := $(shell grep -oP 'GENERATE_LATEX\s*=\s*\K.*' < $($(_DIR_)_CONF_))
endif

$(_DIR_)_DOX_HTML_  := $(if $(filter YES,$(_html_enabled_)),YES,)
$(_DIR_)_DOX_LATEX_ := $(if $(filter YES,$(_latex_enabled_)),YES,)
