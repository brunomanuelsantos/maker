.. Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


=====
Usage
=====

This section focuses on setting up a project to use this build system. But some
experience with it may prove a lot more helpful in understanding by examples.
You can use the ``tests`` directory for this. Remember that some tests may fail
to build on purpose though.

.. tip::

   An easy way to break it in is by exploring available targets with tab
   expansion. Try it!

Back to the subject, the project directory should look something like::

   .
   |-- Makefile -> path/to/build/system/Makefile
   |-- project.mk
   |-- dir.mk
   |
   |-- dir1/
   |   |-- dir.mk
   |   `-- ...
   |
   |-- dir2/
   |   |-- dir.mk
   |   |-- ...
   |   |
   |   `-- dir3/
   |       |-- dir.mk
   |       `-- ...
   |
   `-- ...


Makefiles
=========

The build system relies on simple user makefiles to control it. These do so
through special variables, some part of the core build system, others specific
to the chosen rules. These makefiles are:

``project.mk``

   This holds some project constants like an identification string or some
   options like the build path. Only one ``project.mk`` makefile is ever parsed
   and it must be located at the root of the project, side by side with the
   entry point ``Makefile``.

``dir.mk``

   These are the bread and butter of the build system. These define what needs
   to be compiled and with what rules in any given directory. They also define
   subdirectories to be descended into looking for further ``dir.mk`` files.

   To help the user, the build system will handle paths automatically. Only the
   sources need to be specified, with the added benefit that moving whole
   directories around come at near 0 cost as each folder only knows about
   itself.


Build path
==========

The build system was designed to support both in and out of source builds.
However, note that in general in source building is an inferior option:

* The build system won't be able to automatically maintain a release and debug
  builds side by side.

* The user needs to wipe everything clean every time he wants to switch between
  two configurations or risk a broken build.

* Inferior workflow when using external tools on the source / build directory
  due to additional clutter. E.g. searching specific files.

* Botched builds may leave some clutter behind even after a ``make distclean``.
