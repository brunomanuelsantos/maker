# Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Setup some one off variables
#


# = Functions ================================================================ #

# Load up template. Takes the name of the template as the first parameter.
define ccppasm-template =
	$(eval _TEMPLATE_ := $1)
	$(eval include $(_RULE_DIR_)/c-cpp-asm/template.mk)
	$(eval undefine _TEMPLATE_)
endef


# = Colour codes ============================================================= #

COLOUR_CC  ?= $(COLOUR_BOLD)$(COLOUR_BLUE)


# = Compilation rules ======================================================== #
#
# These rules are immutable, single declaration is sufficient. They need only to
# have the symbol _item_id_ overridden to the respective item when parsing each
# user makefile. This single line suffices:
#
#     $(_DIR_BUILD_)/%: _item_id_ := $(_ITEM_)

# - Object files ------------------------------------------------------------- #

_BIN_DEP_FLAGS_    = -MT $@ -MMD -MP -MF $(basename $@).td
_BIN_POST_COMPILE_ = mv -f $(basename $@).td $(basename $@).d && touch $@

# .S -> .o rule.
$(_BUILDPATH_)/%.o: %.S $(_BUILDPATH_)/%.d
	$(call echo,"AS","$@","$(COLOUR_CC)")
	mkdir -p $(@D)
	$($(_item_id_)_CC_) $($(_item_id_)_CFLAGS_) $(_BIN_DEP_FLAGS_) -c -o $@ $<
	$(_BIN_POST_COMPILE_)

# .c -> .o rule.
$(_BUILDPATH_)/%.o: %.c $(_BUILDPATH_)/%.d
	$(call echo,"CC","$@","$(COLOUR_CC)")
	mkdir -p $(@D)
	$($(_item_id_)_CC_) $($(_item_id_)_CFLAGS_) $(_BIN_DEP_FLAGS_) -c -o $@ $<
	$(_BIN_POST_COMPILE_)

# .cpp -> .o rule.
$(_BUILDPATH_)/%.o: %.cpp $(_BUILDPATH_)/%.d
	$(call echo,"CXX","$@","$(COLOUR_CC)")
	mkdir -p $(@D)
	$($(_item_id_)_CXX_) $($(_item_id_)_CXXFLAGS_) $(_BIN_DEP_FLAGS_) -c -o $@ $<
	$(_BIN_POST_COMPILE_)


# - Disassembly files -------------------------------------------------------- #
#
# Note some of these are virtually useless if LTO is enabled during compilation.
# In that case, a better option is to check the disassembly of the final binary
# or library file or compiling the source to assembly with LTO disabled.
# However, bear in mind that the result still has no connection to the final
# linked code.
#
# Note that using .s here prevents circular dependencies with the .S build rules
# when compiling in source.

# .c -> .s rule.
$(_BUILDPATH_)/%.s: $(_BUILDPATH_)/%.o
	$(call echo,"DISASM","$@","$(COLOUR_CC)")
	mkdir -p $(@D)
	$($(_item_id_)_OBJDUMP_) -S $< > $@

# .a -> .s rule.
$(_BUILDPATH_)/%.s: $(_BUILDPATH_)/%.a
	$(call echo,"DISASM","$@","$(COLOUR_CC)")
	mkdir -p $(@D)
	$($(_item_id_)_OBJDUMP_) -S $< > $@

# .so -> .s rule.
$(_BUILDPATH_)/%.s: $(_BUILDPATH_)/%.so
	$(call echo,"DISASM","$@","$(COLOUR_CC)")
	mkdir -p $(@D)
	$($(_item_id_)_OBJDUMP_) -S $< > $@

# [executable] -> .asm rule. Different extension avoids conflicts when there is
# a .c file with the same name which also qualifies for a disassembly rule.
$(_BUILDPATH_)/%.asm: $(_BUILDPATH_)/%
	$(call echo,"DISASM","$@","$(COLOUR_CC)")
	mkdir -p $(@D)
	$($(_item_id_)_OBJDUMP_) -S $< > $@


# - Include dependency files ------------------------------------------------- #
#
# To build the dependency files exactly as needed without a 2nd pass and without
# extra steps that are not required, we do so as a side effect of compilation.
#
# This is achieved by appending _BIN_DEP_FLAGS_ to the compiler options and
# using _BIN_POST_COMPILE_ after the compilation. These can be seen in the rules
# above and the variables themselves.
#
# The extra options will ensure the dependency file for each individual object
# file is created by the compiler as it produces the actual object file. It also
# creates targets for each dependency so that make doesn't trip when some files
# are deleted.
#
# The post compilation step serves 2 purposes: it will make sure we don't leave
# a bad dependency file behind (file name indirection) and that the object file
# is always newer (touch).
#
# For this section, all that is left to do is declare an empty rule to avoid
# errors and later include all dependency files.

# Empty rule so that make doesn't fail on non existing dependency files.
$(_BUILDPATH_)/%.d: ;

# Don't delete the dependency files that were generated.
.PRECIOUS: $(_BUILDPATH_)/%.d


# = Clean up ================================================================= #

_CLEANUP_ += SRC
_CLEANUP_ += CROSS CC CXX AR LD GDB OBJCPY OBJDUMP
_CLEANUP_ += CFLAGS CXXFLAGS LDFLAGS LIBS
_CLEANUP_ += BIN_FORMAT
