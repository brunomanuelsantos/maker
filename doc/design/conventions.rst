.. Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


===========
Conventions
===========

.. _variables-conventions:

Variables conventions
=====================

Naming convention
~~~~~~~~~~~~~~~~~

The build system requires a large number of variables to be defined a bit all
over the place. This is made more serious due to the non-recursive strategy in
use: i.e. once defined, a variable will apply globally everywhere.

Therefore, the sequence of variable definition, usage and also the type of
assignment are vital for the variables to hold the right value at the right
time.

A few naming conventions should be respected to try and manage this complexity
and provide clear visual clues as to whether it's safe to set or read a variable
in a specific place and reduce the chances of unintentional clashes that would
otherwise be difficult to debug.

In order, from the most useful in user makefiles to the reserved ones, the
conventions are:

``FOO_BAR``

   These denote interface variables, i.e. the variables that describe what is
   being built and how to the build system. These constitute the interface
   between the user application makefiles and the build system.

   Since make uses this style for internal variables, a further restriction
   exists not to write to such existing variables as it may break other parts of
   the build system.

   These are for the exclusive usage as an interface. Both user and build system
   must touch these variables at different times, but declaring new ones is
   naturally reserved to the developer.

``_foo_bar`` or ``_FOO_BAR``

   This is the format the user should use for *any* local variable or macro
   that might be needed outside of the build system. In other words, these are
   the 'do whatever you like' variables for the user.

   These may never be used by the build system developers.

``_foo_bar_``

   Local variable in both the build system and user makefiles. This is not to
   say the same name isn't used across multiple files, but rather that these
   should not be expected to retain its present contents past an ``include``
   directive.

   Since the build system makes this assumption, it is possible for the users to
   use these names as well, so long as they don't use them as global variables
   (the build system may change them) or to inspect the internals of the build
   system (you should use the proper interface variables only).

``foo-bar``

   Functions and macros internal to the build system. These are safe to be
   called in user makefiles, but must not be overridden as the build system uses
   them internally.

   These may only be defined by the build system developers.

``_FOO_BAR_``

   Global variable in the build system. Should be used as sparingly as possible
   even within the build system itself. This choice was deliberate so that the
   namespace remains unpolluted for the user of the build system and for the
   definition of new interface variables.

   In the build system there are many dynamically defined variables which follow
   this pattern but may have different format once expanded: e.g.
   ``$(_FOO_)_BAR_`` expanding into ``foo_BAR_``. These are still considered
   global by the build system.

   These may only be defined and used by the build system developers.

.. warning::
   Technically, *everything* is global in non-recursive make. Nothing prevents
   the user from disrespecting these guidelines, but things may very well break
   in doing so.

.. important::
   If technical advice is not enough of a reason, mind you that the license
   exception in this project pertrains specifically to the interface variables
   (e.g. ``FOO_BAR``) and public functions (e.g. ``foo-bar``).

   Interacting with the build system through any other variables implies the
   revocation of those exceptions.


Special prefixes
~~~~~~~~~~~~~~~~

The naming conventions are very useful to establish boundaries, but forcing the
user to use ``_foo_bar`` or even ``_FOO_BAR`` format for everything that is not
a build system interface may quickly lead to unconventional and ugly looking
makefiles.

To mitigate this somewhat, the build system guarantees not to use variables with
some special prefixes so that the common ``FOO_BAR`` names can be used. Below
are all the prefixes guaranteed not to be used internally by the build system.

* ``G_*``
* ``GLOBAL_*``
* ``CONF_*``
* ``CONFIG_*``

While there is an infinite number of prefixes not currently used by the build
system, this represents a blessed list of prefixes which is to be kept small by
design.

.. note::
   These exceptions are not made with any particular use case in mind. As far as
   the build system is concerned, such variables don't even exist.


Variable assignment
~~~~~~~~~~~~~~~~~~~

Make (GNU Make, but also others) support two types of assignments. These are
documented by make as two 'flavours' of variables:

* Recursively expanded, declared as ``FOO = $(BAR)``.
* Simply expanded, declared as ``FOO := $(BAR)``.

The difference between each one is outside of this documentation scope, but
there is plenty of documentation and practical examples explaining the two
behaviours. Needless to say that some parts of the build system are completely
indifferent to which method is used, and many others rely on a specific method.
Failure to respect the assignments may lead to a broken build system.

The user may use any flavour at will though since all the interface variables
are simply expanded on each directory and then cleared or, in other words, the
build system guarantees the interface variables are simply expanded either way.
