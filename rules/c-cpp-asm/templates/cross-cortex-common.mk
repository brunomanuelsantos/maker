# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Toolchain template for common ARM Cortex cross compilation
#


# Load the default toolchain configuration.
include $(_RULE_DIR_)/c-cpp-asm/templates/gcc-common.mk

CROSS := arm-none-eabi-

CFLAGS   +=
CXXFLAGS +=
LDFLAGS  += -Wl,-Map=$(_DIR_BUILD_)/$(_ITEM_).map
LIBS     +=

ifeq ($(if $(TYPE),$(TYPE),$(_TYPE_)),executable)

LDFLAGS  += -Wl,--print-memory-usage

# Enable OpenOCD subtype and tentatively set interface variables.
SUBTYPES := openocd
LINK     := interface/stlink-v2-1.cfg
ADDRESS  := 0x08000000

endif

BIN_FORMAT := true
