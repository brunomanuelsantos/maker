.. Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
   SPDX-License-Identifier: GPL-3.0-or-later


=======
OpenOCD
=======

.. _openocd-types:

Types
=====

``openocd``

  Define ``openocd-*``, ``flash-*``, ``dbg-*`` and other debug targets differing
  only on the launched front end.


Subtypes
========

``openocd``

   See :ref:`openocd-types`.


Variables
=========

Generic
~~~~~~~


Specific
~~~~~~~~

``CROSS``

  Name prefix of the debugger to be used.

``GDB``

  Name of the debugger to be used.

``ELF``

  Name of the ELF executable. This is required for debug targets. If empty, the
  program is still debugged, but without it, which means no debugging symbols
  will be available, i.e. as if the ``ELF`` had been compiled with ``-g0``.

``BIN``

  Name of the binary file to be flashed.

``OPENOCD``

  Holds the name / path to |openocd|.

``LINK``

  Holds the name / path to |openocd| link specific script.

``TARGET``

  Holds the name / path to |openocd| target specific script.

``ADDRESS``

  Holds the address at which to flash the binary file.


Debug keys
==========

``openocd``

   This agglomerates all debug information related to |openocd| targets.
