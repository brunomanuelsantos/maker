# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Setup some one off variables
#


# = Constants ================================================================ #

# - Custom style ------------------------------------------------------------- #

# By default, include the bundled style sheet. Export so it's accessible by
# Doxygen.
_DOX_HTML_CSS_ := $(call pwd)/override.css
export _DOX_HTML_CSS_


# - Verbosity ---------------------------------------------------------------- #
#
# Doxygen is quite verbose, as well as LaTeX, so we're fixing that for the
# nominal case. Doxygen will still issue warnings and errors and LaTeX is still
# free to fail.
#
# User may also override Doxygen's QUIET setting via a local Doxyfile.

ifneq ($(strip $(_VERBOSITY_)),0)
export _DOX_QUIET_ := NO
_DOX_LATEX_QUIET_  :=
else
export _DOX_QUIET_ := YES
_DOX_LATEX_QUIET_  := &> /dev/null
endif


# - Colour codes ------------------------------------------------------------- #

COLOUR_DOX ?= $(COLOUR_BOLD)$(COLOUR_YELLOW)


# = Clean up ================================================================= #

_CLEANUP_ += CONF
