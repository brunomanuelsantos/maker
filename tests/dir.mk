# Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later

#
# Test makefile
#
# The purpose here is not to test every single feature since that would require
# *a lot* of effort and, in some cases, depend in external devices being
# available or at least emulated. With that said, let's try and cover as much
# ground as possible.
#

SUBDIRS  = bad-c-prog good-c-prog
SUBDIRS += good-c-shared
SUBDIRS += good-c-embed
