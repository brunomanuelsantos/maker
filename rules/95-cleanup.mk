# Copyright (c) 2018 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Clean up all the interface variables
#


# = Wipe all variables that were parsed ====================================== #
#
# All variables were parsed and stored internally for what comes next. This
# sanitizes the environment for the following dir.mk file, if any.
#
# These need immediate expansion to work correctly and must be reset
# *unconditionally*.

# Wipe global interface variables.
undefine SUBDIRS
undefine TYPE
undefine ITEM
undefine SUBTYPES

# Wipe rule interface variables.
$(foreach v,$(_CLEANUP_),$(eval undefine $v))
