# Copyright (c) 2018-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later WITH maker-exception-1.0

#
# Parse user variables
#


# = Setup ==================================================================== #
#
# Whenever a new item is defined, we need to do a bit of setup. A new item has
# been defined if the user has specified 'ITEM' in the current dir.mk file.

ifneq ($(strip $(_USR_ITEM_)),)

# Toolchain.
$(_ITEM_ID_)_CC_      := $(CROSS)$(CC)
$(_ITEM_ID_)_CXX_     := $(CROSS)$(CXX)
$(_ITEM_ID_)_AR_      := $(CROSS)$(AR)
$(_ITEM_ID_)_LD_      := $(CROSS)$(LD)
$(_ITEM_ID_)_OBJCOPY_ := $(CROSS)$(OBJCOPY)
$(_ITEM_ID_)_OBJDUMP_ := $(CROSS)$(OBJDUMP)

# Assume gcc until C++ sources are found.
$(_ITEM_ID_)_BACKEND_ := $($(_ITEM_ID_)_CC_)

# Select output format.
$(_ITEM_ID_)_BIN_FORMAT_ := $(BIN_FORMAT)

$(_ITEM_ID_)_CFLAGS_   :=
$(_ITEM_ID_)_CXXFLAGS_ :=
$(_ITEM_ID_)_LDFLAGS_  :=
$(_ITEM_ID_)_LIBS_     :=

# Initialize item's source lists.
$(_ITEM_ID_)_OBJ_  :=
$(_ITEM_ID_)_DEPS_ :=

endif


# = C / C++ / Assembly sources =============================================== #

# Filter out duplicates.
_src_ := $(call dedup,$(SRC))

# If we find any C++ sources, let's use g++ instead.
ifneq ($(filter %.cpp,$(_src_)),)
$(_ITEM_ID_)_BACKEND_ := $($(_ITEM_ID_)_CXX_)
endif

# Strip file extension and prefix with build path.
_strip_src_ := $(addprefix $(_DIR_BUILD_)/,$(basename $(_src_)))

# Directory target list.
$(_DIR_)_OBJ_  := $(addsuffix .o,$(_strip_src_))
$(_DIR_)_DEPS_ := $(addsuffix .d,$(_strip_src_))
$(_DIR_)_ASM_  := $(addsuffix .s,$(_strip_src_))

# Append to item's target list.
$(_ITEM_ID_)_OBJ_ += $($(_DIR_)_OBJ_)


# = Append user flags ======================================================== #
#
# Item will be compiled with consistent set of flags, yet the user should be
# able to specify flags deep into the source tree.

$(_ITEM_ID_)_CFLAGS_   += $(CFLAGS)
$(_ITEM_ID_)_CXXFLAGS_ += $(CXXFLAGS)
$(_ITEM_ID_)_LDFLAGS_  += $(LDFLAGS)
$(_ITEM_ID_)_LIBS_     += $(LIBS)
